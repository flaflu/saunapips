//+------------------------------------------------------------------+
//|                                                    SaunaPips.mq5 |
//|                                                        LuizeraSD |
//|                                                       SaunaForex |
//+------------------------------------------------------------------+
#include <MovingAverages.mqh>

#property copyright "LuizeraSD"
#property link      "https://saunaforex.000webhostapp.com"
#property version   "1.00"

enum intOptionsTrade
{
   A1 = 1,//in favor
   A2 = 2,//against
   A3 = 3,//both
   A4 = 4,//both reversal
   A5 = 5,//both simultaneous
};


//--- input parameters
input string TradeComment = "SaunaPips v1.04"; 
input int MagicNumber = 171717;
input int MaxSlippage = 3;
input int MaxSpread = 5;
input int MaxAvgSpread = 20;
input double FixedLot = 0;
input int RiskPercent = 30;
input int StartHour = 0;
input int EndHour = 23;
input int GMTOffset = 0;

input int maxBuyStopOrders = 1;
input int maxSellStopOrders = 1;
input int maxOpenBuy = 5;
input int maxOpenSell = 5;

input int VelocityRange = 90; 
input int VelocityStop = 20; 

//Tempo para atualizar o valor do rateChange
input int VelocityTime_ms = 3000; 

//Tempo de vida da ordem pendente
input int ExpirationStopOrder_seconds = 10;

//Distancia em pontos/pippets que vai abrir a Stop Order do preco atual
input int Velocity_StopOrderPoints = 10; 

//Distancia em pontos/pippets que o StopLoss vai ficar do preco apos a abertura da ordem
input int Velocity_StopLossTrail = 20; 

//Distancia em pontos/pippets maxima que o Trail do SL vai andar a favor do trade (lucro)
input int Velocity_TrailHold = 40; 

//Tempo em minutos que o trade vai aguardar pra poder ultrapassar o Trail do SL alem do Velocity_TrailHold
input int Velocity_TrailResume = 0;

//Stop Loss sera ativado nesse nivel em pontos caso alcance o lucro minimo
input int Velocity_StopLossTarget = 0;

input int AccelerationTarget = 0;

input intOptionsTrade TradeTendency = A3;

input bool OperateMonday = 1;
input bool OperateTuesday = 1;
input bool OperateWednesday = 1;
input bool OperateThursday = 1;
input bool OperateFriday = 1;

input bool DebugMode = 0;

//--- internal Variables
int TickSample = 100;
int AccelerationSample = 20;

MqlTick last_tick; 

int r, gmt, brokerOffset, stoplevel, currentSpread, velocityRecord;

double marginRequirement, maxLot, minLot, lotSize, points, initialBalance, avgSpread, avgAcceleration, accelerationUp, accelerationDown, rateChange, commissionPoints, stopOrderPoints, stopLossTrail;

double arrSpreadSize[]; 
double arrTick[];
double arrAvgtick[];
double arrAvgAcceleration[];
double arrAccelerationUp[];
double arrAccelerationDown[];
long arrTickTime[];

double priceThen = 0; //Last know bid on Velocity_Time seconds ago

string testerStartDate, testerEndDate; 

int lastBuyOrder, lastSellOrder, totalBuyStop, totalSellStop, totalBuy, totalSell;

bool calculateCommission = true;

uint speedSpreadCalc;

long tickTimeDif;
double tickDif, newAcceleration, accelerationRecord, volatility;


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   Print("Vamo começa");
   marginRequirement = GetMarginRequired(Symbol());
   //marginRequirement =  SymbolInfoDouble(Symbol(), SYMBOL_MARGIN_MAINTENANCE);
   //OrderCalcMargin(ORDER_TYPE_BUY, Symbol(), 1, SymbolInfoDouble(Symbol(),SYMBOL_BID), marginRequirement);

   maxLot = SymbolInfoDouble(Symbol(),SYMBOL_VOLUME_MAX);
   minLot = SymbolInfoDouble(Symbol(),SYMBOL_VOLUME_MIN);
   stoplevel = ( int ) MathMax( SymbolInfoInteger(Symbol(),SYMBOL_TRADE_FREEZE_LEVEL), SymbolInfoInteger(Symbol(),SYMBOL_TRADE_STOPS_LEVEL));
   
   if(stoplevel > Velocity_StopOrderPoints) 
      stopOrderPoints = stoplevel;
   else    
      stopOrderPoints = Velocity_StopOrderPoints;
      
   if(stoplevel > Velocity_StopLossTrail) 
      stopLossTrail = stoplevel;
   else
      stopLossTrail = Velocity_StopLossTrail;
   
   avgSpread = SymbolInfoInteger(Symbol(),SYMBOL_SPREAD);
   //avgSpread = NormalizeDouble( SymbolInfoDouble(Symbol(),SYMBOL_ASK) - SymbolInfoDouble(Symbol(),SYMBOL_BID), Digits());
   Print("First ArraySpread = ",DoubleToString( avgSpread/Point() ,0 ));
   
   ArrayResize( arrSpreadSize, TickSample ); 
   ArrayFill( arrSpreadSize, 0, TickSample, avgSpread );
   
   ArrayResize( arrAvgAcceleration, AccelerationSample);
   ArrayFill( arrAvgAcceleration, 0, AccelerationSample, 0);
   
   ArrayResize( arrAccelerationUp, AccelerationSample);
   ArrayFill( arrAccelerationUp, 0, AccelerationSample, 0);
   ArrayResize( arrAccelerationDown, AccelerationSample);
   ArrayFill( arrAccelerationDown, 0, AccelerationSample, 0);
   
   testerStartDate = TimeToString(TimeCurrent(), TIME_DATE);
   initialBalance = AccountInfoDouble(ACCOUNT_BALANCE);
   
   display();
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---   

   if(!SymbolInfoTick(Symbol(),last_tick)){
      Print("SymbolInfoTick() failed, error = ",GetLastError());
      return;
   }
   
   if( calculateCommission ) commission();
   
   //currentSpread = (last_tick.ask - last_tick.bid)/Point();
   currentSpread = SymbolInfoInteger(Symbol(),SYMBOL_SPREAD);
   
   
   totalBuyStop = 0;
   totalSellStop = 0;
   totalBuy = 0;
   totalSell = 0;
   
   double maxStopLoss;
   double newStopLoss;
   double newPrice;   
   
   gmt = offlineGMT();
   prepareSpread(); //Atualiza a media do spread (avgSpread)
   manageTicks();  //Atualiza o valor da oscilacao (rateChange)
   manageAcceleration();  //Atualiza o valor da aceleracao (avgAcceleration)
   
   
   for( int pos = 0; pos < OrdersTotal(); pos++ ) {
      ulong ticket=OrderGetTicket(pos);
      if(ticket!=0){
         if( OrderGetString(ORDER_SYMBOL) != Symbol() ) continue;
         if( OrderGetInteger(ORDER_MAGIC) == MagicNumber ){         
            //double ask = SymbolInfoDouble(Symbol(),SYMBOL_ASK);
            //double bid = SymbolInfoDouble(Symbol(),SYMBOL_BID);
            double sl = NormalizeDouble(OrderGetDouble(ORDER_SL), Digits());
            double tp = NormalizeDouble(OrderGetDouble(ORDER_TP), Digits());
            double openPrice = NormalizeDouble(OrderGetDouble(ORDER_PRICE_OPEN), Digits());
            //datetime openTime = OrderGetInteger(ORDER_TIME_DONE);
            
            switch ( OrderGetInteger(ORDER_TYPE) ) {
               case ORDER_TYPE_BUY_STOP:
                  if( (int) TimeCurrent() - lastBuyOrder > ExpirationStopOrder_seconds ){
                     deleteOrder(ticket);
                  }else if( openPrice - last_tick.ask > Velocity_StopOrderPoints * Point()){
                     newPrice = NormalizeDouble( last_tick.ask + ( totalBuyStop + 1.0 ) * ( Velocity_StopOrderPoints * Point() ), Digits() );                     
                     if(newPrice != openPrice)
                        modifyOrder(ticket, newPrice, sl, tp);
                  }
                  totalBuyStop++;
               break;
               case ORDER_TYPE_SELL_STOP:
                  if( (int) TimeCurrent() - lastSellOrder > ExpirationStopOrder_seconds ){
                     deleteOrder(ticket);
                  } else if( last_tick.bid - openPrice > Velocity_StopOrderPoints * Point()) {
                     newPrice = NormalizeDouble( last_tick.bid - ( totalSellStop + 1.0 ) * ( Velocity_StopOrderPoints * Point() ), Digits());
                     if(newPrice != openPrice)
                        modifyOrder(ticket, newPrice, sl, tp);
                  }
                  totalSellStop++;
               break;
            }
         }
      }
   }
      
   for( int pos = 0; pos < PositionsTotal(); pos++ ) {
      ulong ticket=PositionGetTicket(pos);
      if(ticket!=0){   
         if( PositionGetString(POSITION_SYMBOL) != Symbol() ) continue;
            if( PositionGetInteger(POSITION_MAGIC) == MagicNumber ){
               //double ask = SymbolInfoDouble(Symbol(),SYMBOL_ASK);
               //double bid = SymbolInfoDouble(Symbol(),SYMBOL_BID);
               double sl =  NormalizeDouble(PositionGetDouble(POSITION_SL), Digits());
               double tp = NormalizeDouble(PositionGetDouble(POSITION_TP), Digits());
               double openPrice = NormalizeDouble(PositionGetDouble(POSITION_PRICE_OPEN), Digits());
               datetime openTime = PositionGetInteger(POSITION_TIME);
               
               
               switch ( PositionGetInteger(POSITION_TYPE) ) {
               case POSITION_TYPE_BUY:
                  
                  if(sl == 0.0 || last_tick.bid - sl > Velocity_StopLossTrail * Point() ){                     
                     
                     bool hitStopLossTarget = (Velocity_StopLossTarget > 0 && last_tick.bid - openPrice > Velocity_StopLossTarget*Point());
                     
                     //Idade em segundos da ordem for menor que o Velocity_TrailResume
                     bool holdTimerON = ((int)TimeCurrent() - (int)openTime < Velocity_TrailResume*60);                     
                     
                     if( holdTimerON && !hitStopLossTarget){
                        
                        maxStopLoss = NormalizeDouble(openPrice + (Velocity_TrailHold  * Point()), Digits());
                        newStopLoss = MathMin(maxStopLoss, NormalizeDouble(last_tick.bid - (Velocity_StopLossTrail * Point()), Digits()));
                                             
                        //Atualiza para o newStopLoss somente se estiver dentro do limite do Velocity_TrailHold
                        if(newStopLoss != sl && newStopLoss <= maxStopLoss){
                           modifyPosition(ticket, newStopLoss, tp);
                        }
                     }else{
                        newStopLoss = NormalizeDouble(last_tick.bid - (Velocity_StopLossTrail * Point()), Digits());
                                                                     
                        //Ja passou o TrailResumo, pode ir ajustando o SL da ordem alem do TrailHold
                        modifyPosition(ticket, newStopLoss, tp);
                     }
                  }
                  totalBuy++;
               break;
               case POSITION_TYPE_SELL:
                  
                  if(sl == 0.0 || sl - last_tick.ask >  Velocity_StopLossTrail * Point()){
                  
                     bool hitStopLossTarget = (Velocity_StopLossTarget > 0 && openPrice - last_tick.ask > Velocity_StopLossTarget*Point());
                     
                     //Idade em segundos da ordem for menor que o Velocity_TrailResume
                     bool holdTimerON = ((int)TimeCurrent() - (int)openTime < Velocity_TrailResume*60);                     
                     
                     if( holdTimerON && !hitStopLossTarget){
                        
                        maxStopLoss = NormalizeDouble(openPrice - (Velocity_TrailHold  * Point()), Digits());
                        newStopLoss = MathMax(maxStopLoss, NormalizeDouble(last_tick.ask + (Velocity_StopLossTrail * Point()), Digits()));
                        
                        //Atualiza para o newStopLoss somente se estiver dentro do limite do Velocity_TrailHold
                        if(newStopLoss != sl && newStopLoss >= maxStopLoss){
                           modifyPosition(ticket, newStopLoss, tp);
                        }
                     }else{
                        newStopLoss = NormalizeDouble(last_tick.ask + (Velocity_StopLossTrail * Point()), Digits());
                        //Ja passou o TrailResumo, pode ir ajustando o SL da ordem alem do TrailHold
                        modifyPosition(ticket, newStopLoss, tp);
                     }
                  }
                  totalSell++;
               break;
            }
         }
      }
   }
   
   //Aqui que ele abre as novas posicoes conforme as condicoes abaixo
   if( ( ( StartHour < EndHour && gmt >= StartHour && gmt <= EndHour ) 
   || ( StartHour > EndHour && ( ( gmt <= EndHour && gmt >= 0 ) 
   || ( gmt <= 23 && gmt >= StartHour ) ) ) ) 
   ){
      MqlDateTime tm;
      TimeCurrent(tm);
      if((tm.day_of_week == 1 && OperateMonday) || (tm.day_of_week == 2 && OperateTuesday)
      || (tm.day_of_week == 3 && OperateWednesday) || (tm.day_of_week == 4 && OperateThursday)
      || (tm.day_of_week == 5 && OperateFriday)
      ){

         if(currentSpread <= MaxSpread && avgSpread <= MaxAvgSpread*Point() ){
            
            bool velocityCondition = (VelocityRange > 0 && MathAbs(rateChange) > VelocityRange * Point()); //Se atingir o TriggerVelocity Ex: ( 90 - 20 = 70)
            bool accelerationCondition = (AccelerationTarget > 0 && (accelerationUp > AccelerationTarget || MathAbs(accelerationDown) > AccelerationTarget));
            //bool accelerationCondition = (AccelerationTarget > 0 && avgAcceleration > AccelerationTarget);
            
            //if(MathAbs(rateChange) > MathAbs(VelocityRange - VelocityStop)* Point() || 
            if(velocityCondition || accelerationCondition){
            
               if(TradeTendency == 5){ //Open simultaneous orders
                  if(totalBuyStop < maxBuyStopOrders && totalBuy < maxOpenBuy)
                     buy(totalBuyStop);               
                  if(totalSellStop < maxSellStopOrders && totalSell < maxOpenSell)
                     sell(totalSellStop);
               }else{               
                  bool tradeInFavor = (TradeTendency == 1 || (TradeTendency >= 3 && ((VelocityRange > 0 && MathAbs(rateChange) > VelocityRange * Point()) || accelerationCondition)));
                  //bool tradeInFavor = (TradeTendency == 1 || (TradeTendency >= 3 && volatility < 1000));
                  
                  if(TradeTendency == 4)
                     tradeInFavor = !(tradeInFavor);
                     
                  bool triggerBuy = false;
                  bool triggerSell = false;
                  
                  bool bulish = ((VelocityRange > 0 && rateChange > 0) || (AccelerationTarget > 0 && accelerationUp > AccelerationTarget));
                  bool bearish = ((VelocityRange > 0 && rateChange < 0) || (AccelerationTarget > 0 && MathAbs(accelerationDown) > AccelerationTarget));
                  
                  //Se for volatilidade Bulish
                  if(bulish){
                     if(tradeInFavor){
                        triggerBuy = true; //Segue Tendencia Bullish
                     }else{
                        triggerSell = true; //ContraTendencia Bullish
                     }
                  }else if(bearish){ //Se for volatilidade Bearish
                     if(tradeInFavor){
                        triggerSell = true; //Segue Tendencia Bearish
                     }else{
                        triggerBuy = true; //ContraTendencia Bearish
                     }
                  }
                  
                  if(triggerBuy && totalBuyStop < maxBuyStopOrders && totalBuy < maxOpenBuy)
                     buy(totalBuyStop);
                  
                  if(triggerSell && totalSellStop < maxSellStopOrders && totalSell < maxOpenSell)
                     sell(totalSellStop);
               }               
            }
         }
      }   
   }
    
   display();    
  }
//+------------------------------------------------------------------+

void buy(int buyStop){
   double buyprice = NormalizeDouble(last_tick.ask + ( buyStop + 1.0 ) * Velocity_StopOrderPoints * Point(), Digits());
   
   string comment = "";
   StringConcatenate(comment, "X:",IntegerToString(buyStop),",",getDefaultComment());   
   
   MqlTradeRequest request={0};
   MqlTradeResult  result={0};

   request.action=TRADE_ACTION_PENDING;
   request.magic=MagicNumber;
   request.price=buyprice;
   request.symbol=Symbol(); 
   request.volume= lotSize();
   request.deviation= MaxSlippage;
   request.type=ORDER_TYPE_BUY_STOP;
   request.comment= comment;

   if(!OrderSend(request,result)){
      PrintFormat("OrderSend error %d",GetLastError());
      string err = "";
      StringConcatenate(err,"FAILED to open BuyStop at:",DoubleToString(buyprice,5)," | ",comment);
      Print(err);
   }
   PrintFormat("retcode=%u  deal=%I64u  order=%I64u",result.retcode,result.deal,result.order);
   string err2 = "";
   StringConcatenate(err2," => Opened BuyStop at:",DoubleToString(buyprice,5)," | ",comment);
   Print(err2);
   
   ZeroMemory(request);
   ZeroMemory(result);
   
   lastBuyOrder = ( int ) TimeCurrent();
}

void sell(int sellStop){
   double sellprice =  NormalizeDouble(last_tick.bid - (sellStop + 1) * Velocity_StopOrderPoints * Point(), Digits());
   
   string comment = "";
   StringConcatenate(comment, "X:",IntegerToString(sellStop),",",getDefaultComment());
   
   MqlTradeRequest request={0};
   MqlTradeResult  result={0};

   request.action=TRADE_ACTION_PENDING;
   request.magic=MagicNumber;
   request.price=sellprice;
   request.symbol=Symbol();
   request.volume= lotSize();
   request.deviation= MaxSlippage;
   request.type=ORDER_TYPE_SELL_STOP;
   request.comment= comment;

   if(!OrderSend(request,result)){
      PrintFormat("OrderSend error %d",GetLastError());
      string err = "";
      StringConcatenate(err,"FAILED to open SellStop at:",DoubleToString(sellprice,5)," | ",comment);
      Print(err);
   }
   PrintFormat("retcode=%u  deal=%I64u  order=%I64u",result.retcode,result.deal,result.order);
   string err2 = "";
   StringConcatenate(err2," => Opened SellStop at:",DoubleToString(sellprice,5)," | ",comment);
   Print(err2);
   
   ZeroMemory(request);
   ZeroMemory(result);
         
   lastSellOrder = ( int ) TimeCurrent();
}

string getDefaultComment(){
   string res;
   StringConcatenate(res, "V:"+DoubleToString(rateChange/Point(),0),",S:",DoubleToString(avgSpread/Point(),0)," | ",TradeComment);
   return res;
}

void deleteOrder(ulong ticket){
   if(!OrderSelect(ticket)){
      PrintFormat("Could not find ticket %d to delete", ticket);
      return;
   }
   
   MqlTradeRequest request={0};
   MqlTradeResult  result={0};

   request.action=TRADE_ACTION_REMOVE;
   request.magic= OrderGetInteger(ORDER_MAGIC);
   request.order=ticket;
   request.symbol=Symbol(); 
   request.volume= OrderGetDouble(ORDER_VOLUME_CURRENT);
   request.type=ORDER_TYPE_SELL_STOP;
   request.comment= OrderGetString(ORDER_COMMENT);

   if(!OrderSend(request,result))
      PrintFormat("OrderSend error %d",GetLastError());
   PrintFormat("retcode=%u  deal=%I64u  order=%I64u",result.retcode,result.deal,result.order);
   
   ZeroMemory(request);
   ZeroMemory(result);
}

void modifyOrder(ulong ticket, double price, double sl, double tp){
   if(!OrderSelect(ticket)){
      PrintFormat("Could not find ticket %d to modify", ticket);
      return;
   }
   
   MqlTradeRequest request={0};
   MqlTradeResult  result={0};
   
   int digits=SymbolInfoInteger(Symbol(),SYMBOL_DIGITS);
   request.action=TRADE_ACTION_MODIFY;   
   request.order=ticket; 
   request.symbol=Symbol();
   request.deviation=MaxSlippage;   
   request.tp = NormalizeDouble(tp,digits);
   request.sl = NormalizeDouble(sl,digits);
   request.price    =NormalizeDouble(price,digits);
     
   if(!OrderSend(request,result))
      PrintFormat("OrderSend error %d",GetLastError());  // if unable to send the request, output the error code
   //--- information about the operation   
   PrintFormat("retcode=%u  deal=%I64u  order=%I64u",result.retcode,result.deal,result.order);
   
   ZeroMemory(request);
   ZeroMemory(result);
}

void modifyPosition(ulong ticket, double sl, double tp){
   if(!PositionSelectByTicket(ticket)){
      PrintFormat("Could not find position with ticket %d to modify", ticket);
      return;
   }
   
   MqlTradeRequest request={0};
   MqlTradeResult  result={0};
   
   int digits=SymbolInfoInteger(Symbol(),SYMBOL_DIGITS);
   request.action=TRADE_ACTION_SLTP;
   request.position=ticket;
   request.symbol=Symbol();
   request.tp = NormalizeDouble(tp,digits);
   request.sl = NormalizeDouble(sl,digits);

   if(!OrderSend(request,result))
      PrintFormat("OrderSend error %d",GetLastError());  // if unable to send the request, output the error code
   //--- information about the operation   
   PrintFormat("retcode=%u  deal=%I64u  order=%I64u",result.retcode,result.deal,result.order);   
   
   ZeroMemory(request);
   ZeroMemory(result);
}

void prepareSpread(){   
   double spreadSize_temp[];
   ArrayResize( spreadSize_temp, TickSample - 1 );
   ArrayCopy( spreadSize_temp, arrSpreadSize, 0, 1, TickSample - 1 );
   ArrayResize( spreadSize_temp, TickSample );
   //spreadSize_temp[TickSample-1] = NormalizeDouble( SymbolInfoDouble(Symbol(),SYMBOL_ASK) - SymbolInfoDouble(Symbol(),SYMBOL_BID), Digits());
   spreadSize_temp[TickSample-1] = NormalizeDouble( currentSpread*Point(), Digits());
   ArrayCopy( arrSpreadSize, spreadSize_temp, 0, 0 );
   avgSpread = LinearWeightedMA(TickSample-1, TickSample-1, arrSpreadSize);
}       

void manageTicks(){
   /*uint start=GetTickCount(); 
   speedSpreadCalc=GetTickCount()-start;
   PrintFormat("Calculating took %d ms",speedSpreadCalc);*/
   
   double tick_temp[], avgtick_temp[];
   
   long tickTime_temp[];
   ArrayResize( tick_temp, TickSample - 1 );
   ArrayResize( tickTime_temp, TickSample - 1 );   
   
   ArrayCopy( tick_temp, arrTick, 0, 1, TickSample - 1 ); 
   ArrayCopy( tickTime_temp, arrTickTime, 0, 1, TickSample - 1 );    
   
   ArrayResize( tick_temp, TickSample ); 
   ArrayResize( tickTime_temp, TickSample );   
   
   tick_temp[TickSample-1] = NormalizeDouble(last_tick.bid,Digits());
   tickTime_temp[TickSample-1] = last_tick.time_msc;
   ArrayCopy( arrTick, tick_temp, 0, 0 );
   ArrayCopy( arrTickTime, tickTime_temp, 0, 0 );
   
   long timeNow = arrTickTime[TickSample-1];
   double priceNow = arrTick[TickSample-1];
   int period = 0;
   for( int i = TickSample - 1; i >= 0; i-- ){
      period++;
      if( timeNow - arrTickTime[i] > VelocityTime_ms ){
         priceThen = arrTick[i]; 
         break;
      }
   }
    
   rateChange = ( priceNow - priceThen );
   
   if( rateChange / Point() > 5000 ) rateChange = 0; //Limit Protection
   
   if(rateChange/Point() > velocityRecord){
      velocityRecord = rateChange/Point();
   }
   
   double tickSum = 0;
   for( int i = TickSample - 1; i >= 0; i-- ){
      tickSum += MathAbs(arrTick[i]);
   }
   volatility = NormalizeDouble((tickSum/(arrTickTime[TickSample-1] - arrTickTime[0]))/Point(),2);
}


void manageAcceleration() {
   double avgAcceleration_temp[];
   ArrayResize( avgAcceleration_temp, AccelerationSample - 1 );
   ArrayCopy( avgAcceleration_temp, arrAvgAcceleration, 0, 1, AccelerationSample - 1 );
   ArrayResize( avgAcceleration_temp, AccelerationSample );
   tickTimeDif = arrTickTime[TickSample-1] - arrTickTime[TickSample-2];
   tickDif = (arrTick[TickSample-1] - arrTick[TickSample-2])/Point();
   if(tickTimeDif > 0 && tickDif != 0){
      newAcceleration = tickDif/tickTimeDif;
      avgAcceleration_temp[AccelerationSample-1] = newAcceleration;
      ArrayCopy( arrAvgAcceleration, avgAcceleration_temp, 0, 0 );
      avgAcceleration = LinearWeightedMA(AccelerationSample-1, AccelerationSample-1, arrAvgAcceleration)*100;
      
      
      
      if(avgAcceleration > accelerationRecord) accelerationRecord = avgAcceleration;
      if(tickDif > 0){
         double accelerationUp_temp[];
         ArrayResize( accelerationUp_temp, AccelerationSample - 1 );
         ArrayCopy( accelerationUp_temp, arrAccelerationUp, 0, 1, AccelerationSample - 1 );
         ArrayResize( accelerationUp_temp, AccelerationSample );
         accelerationUp_temp[AccelerationSample-1] = tickDif;
         ArrayCopy( arrAccelerationUp, accelerationUp_temp, 0, 0 );            
         accelerationUp = LinearWeightedMA(AccelerationSample-1, AccelerationSample-1, arrAccelerationUp)*100;
      }else if(tickDif < 0){
         double accelerationDown_temp[];
         ArrayResize( accelerationDown_temp, AccelerationSample - 1 );
         ArrayCopy( accelerationDown_temp, arrAccelerationDown, 0, 1, AccelerationSample - 1 );
         ArrayResize( accelerationDown_temp, AccelerationSample );
         accelerationDown_temp[AccelerationSample-1] = tickDif;
         ArrayCopy( arrAccelerationDown, accelerationDown_temp, 0, 0 );            
         accelerationDown = LinearWeightedMA(AccelerationSample-1, AccelerationSample-1, arrAccelerationDown)*100;
      }
   }
}

double GetMarginRequired( const string Symb )
{
  MqlTick Tick;
  double MarginInit, MarginMain;

  return((SymbolInfoTick(Symb, Tick) && SymbolInfoMarginRate(Symb, ORDER_TYPE_BUY, MarginInit, MarginMain)) ? MarginInit * Tick.ask *
          SymbolInfoDouble(Symb, SYMBOL_TRADE_TICK_VALUE) / (SymbolInfoDouble(Symb, SYMBOL_TRADE_TICK_SIZE) * AccountInfoInteger(ACCOUNT_LEVERAGE)) : 0);
}

double lotSize(){  
   if( FixedLot > 0 ){
      lotSize = NormalizeDouble( FixedLot, 2 );
   } else {
      if( marginRequirement > 0 ){
         double acBalance = AccountInfoDouble(ACCOUNT_BALANCE);
         double lotS = NormalizeDouble( ( acBalance * ( ( double ) RiskPercent / 10 ) * 0.01 / marginRequirement ), 2 );
         lotSize = MathMax( MathMin( lotS, maxLot ), minLot );    
      }
   }
   return ( NormalizeLots( lotSize ) ); 
}

double NormalizeLots( double p ){
    double ls = SymbolInfoDouble( Symbol(), SYMBOL_VOLUME_STEP );
    return( MathRound( p / ls ) * ls );
}

void commission(){
   int Deals_Counter=0;
   int Position_Counter=0;
   
   HistorySelect(0,TimeCurrent());
   int deals = HistoryDealsTotal();
   
   for(int i = 0; i < deals; i++){
      ulong deal_ticket=HistoryDealGetTicket(i);
      
      if( HistoryDealGetString(deal_ticket, DEAL_SYMBOL) != Symbol()) continue;
      
      if(HistoryDealGetInteger(deal_ticket,DEAL_POSITION_ID)!=0 && HistoryDealGetInteger(deal_ticket,DEAL_ENTRY)==DEAL_ENTRY_IN){
         //--- Entry deal for position
         Deals_Counter=1;
         long     Position_ID     = HistoryDealGetInteger(deal_ticket, DEAL_POSITION_ID);
         double   open_price      = NormalizeDouble(HistoryDealGetDouble(deal_ticket, DEAL_PRICE), Digits());
         double   deal_volume     = HistoryDealGetDouble(deal_ticket, DEAL_VOLUME);
         double   deal_commission = HistoryDealGetDouble(deal_ticket, DEAL_COMMISSION);
         double   deal_profit     = HistoryDealGetDouble(deal_ticket, DEAL_PROFIT);

         //--- Find exit deal(s) for position
         long     close_time         = 0;
         double   close_price        = 0;
         double   close_volume       = 0;
         double   weighted_sum_price = 0;
         //---
         for(int j=i+1; j<deals; j++) {
            deal_ticket=HistoryDealGetTicket(j);
            if(HistoryDealGetInteger(deal_ticket,DEAL_POSITION_ID)==Position_ID && (HistoryDealGetInteger(deal_ticket,DEAL_ENTRY)==DEAL_ENTRY_OUT || HistoryDealGetInteger(deal_ticket,DEAL_ENTRY)==DEAL_ENTRY_OUT_BY)){
               Deals_Counter++;
               if(Deals_Counter==2) Position_Counter++;
               deal_commission    += HistoryDealGetDouble(deal_ticket, DEAL_COMMISSION);
               deal_profit        += HistoryDealGetDouble(deal_ticket, DEAL_PROFIT);
               weighted_sum_price += HistoryDealGetDouble(deal_ticket, DEAL_VOLUME) * HistoryDealGetDouble(deal_ticket, DEAL_PRICE);
               close_volume       += HistoryDealGetDouble(deal_ticket, DEAL_VOLUME);
               close_price         = NormalizeDouble(weighted_sum_price / close_volume, Digits());
            }
            //--- if the whole position is closed
            if(MathAbs(close_volume-deal_volume)<0.00001) break;
         }

         //--- Calculate Comission in Points from this Closed Position
         if(Deals_Counter>=2){            
            if( deal_profit != 0.0 ) {
               if( close_price != open_price ) {
                  calculateCommission = false;
                  double rate = MathAbs( deal_profit / MathAbs( close_price - open_price));
                  commissionPoints = NormalizeDouble(( -deal_commission ) / rate, Digits());
                  break;
               }         
            }
         }
      }
   }
}

int offlineGMT(){
   MqlDateTime tm;
   TimeCurrent(tm);

   int gOffset =  tm.hour - GMTOffset; 
   if( gOffset < 0 ) gOffset += 24;
   else if( gOffset > 23 ) gOffset -= 24;
   return ( gOffset );
}

string niceHour( int kgmt ){ 
   string m;
   if( kgmt > 12 ) {
      kgmt = kgmt - 12;
      m = "PM"; 
   } else {
      m = "AM";
      if( kgmt == 12 )
         m = "PM";
   }
   string res;
   StringConcatenate( res, IntegerToString(kgmt), m );
   return res;
}

void display(){  
    if( !MQLInfoInteger(MQL_TESTER) || (MQLInfoInteger(MQL_TESTER) && MQLInfoInteger(MQL_VISUAL_MODE))){
      string displayStr = "  Sauna Pips v1.00\n";
      if(DebugMode){
         StringConcatenate( displayStr, displayStr, " ----------  D  E  B  U  G  ---------", "\n" );
         StringConcatenate( displayStr, displayStr, " RateChange: ", DoubleToString( rateChange, Digits() ), " \n" );
         StringConcatenate( displayStr, displayStr, " VelocityRecord: ", IntegerToString(velocityRecord), " \n" );
         StringConcatenate( displayStr, displayStr, " AccelerationRecord: ", DoubleToString( accelerationRecord,5 ), "\n" );         
         StringConcatenate( displayStr, displayStr, " Total Buy Trades: " , IntegerToString(totalBuy), " \n" );
         StringConcatenate( displayStr, displayStr, " Total Sell Trades: " , IntegerToString(totalSell), " \n" );
         StringConcatenate( displayStr, displayStr," TotalPending Trades: " , IntegerToString(totalBuyStop+totalSellStop), " \n" );         
         StringConcatenate( displayStr, displayStr, " TimeCurrent(ms): " , IntegerToString(last_tick.time_msc), " \n" );
      
         /*string string_arraySpread;
         for( int i = 0; i < ArraySize(arrSpreadSize); i++ ){
            StringConcatenate(string_arraySpread, string_arraySpread, DoubleToString(arrSpreadSize[i]/Point(),0), ",");
         }
         StringConcatenate( displayStr, displayStr, "Array Spread: ", string_arraySpread, " \n" );
         
         string string_arrayTick;
         for( int i = 0; i < ArraySize(arrTick); i++ ){
            StringConcatenate(string_arrayTick, string_arrayTick, DoubleToString(arrTick[i],Digits()), ",");
         }
         StringConcatenate( displayStr, displayStr, "Array Tick: ", string_arrayTick, " \n" );
         
         string string_arrayTickTime;
         for( int i = 0; i < ArraySize(arrTickTime); i++ ){
            StringConcatenate(string_arrayTickTime, string_arrayTickTime, IntegerToString(arrTickTime[i],0), ",");
         }
         StringConcatenate( displayStr, displayStr, "Array TickTime: ", string_arrayTickTime, " \n" );*/
         
         if(ArraySize(arrTick) == TickSample)
            StringConcatenate( displayStr, displayStr, "Last TickDif: ", arrTick[TickSample-1]," - ",arrTick[TickSample-2],"=",DoubleToString((arrTick[TickSample-1]- arrTick[TickSample-2])/Point(),2), " \n" );
         
         if(ArraySize(arrTickTime) == TickSample)
            StringConcatenate( displayStr, displayStr, "Last TickTime: ", IntegerToString(arrTickTime[TickSample-1]- arrTickTime[TickSample-2]), " \n" );
         
         StringConcatenate( displayStr, displayStr, "TickTimeDif: ", IntegerToString(tickTimeDif), " \n" );
         StringConcatenate( displayStr, displayStr, "TickDif: ", DoubleToString(tickDif), " \n" );
         StringConcatenate( displayStr, displayStr, "newAcceleration: ", DoubleToString(newAcceleration), " \n" );         
         
         /*string string_arrayAvgAcceleration;
         for( int i = 0; i < ArraySize(arrAvgAcceleration); i++ ){
            StringConcatenate(string_arrayAvgAcceleration, string_arrayAvgAcceleration, DoubleToString(arrAvgAcceleration[i],5), ",");
         }
         StringConcatenate( displayStr, displayStr, "Array AvgAcceleration: ", string_arrayAvgAcceleration, " \n" );
         
         string string_arrayAccelerationUp;
         for( int i = 0; i < ArraySize(arrAccelerationUp); i++ ){
            StringConcatenate(string_arrayAccelerationUp, string_arrayAccelerationUp, DoubleToString(arrAccelerationUp[i],5), ",");
         }
         StringConcatenate( displayStr, displayStr, "Array AvgAcceleration   UP: ", string_arrayAccelerationUp, " \n" );
         
         string string_arrayAccelerationDown;
         for( int i = 0; i < ArraySize(arrAccelerationDown); i++ ){
            StringConcatenate(string_arrayAccelerationDown, string_arrayAccelerationDown, DoubleToString(arrAccelerationDown[i],5), ",");
         }
         StringConcatenate( displayStr, displayStr, "Array AvgAcceleration DOWN: ", string_arrayAccelerationDown, " \n" );*/
      }
      StringConcatenate( displayStr, displayStr, " ----------------------------------------", "\n" );      
      //StringConcatenate( displayStr, displayStr, " SpeedSpreadCalc: ",IntegerToString(speedSpreadCalc), "\n" );
      StringConcatenate( displayStr, displayStr, "  Equity: ", DoubleToString(AccountInfoDouble(ACCOUNT_EQUITY), 2),"\n" );
      StringConcatenate( displayStr, displayStr, "  Leverage: ", IntegerToString( AccountInfoInteger(ACCOUNT_LEVERAGE)), " Lots: ", DoubleToString( lotSize(), 2 ), ", \n" ); 
      StringConcatenate( displayStr, displayStr, "  Margin Required: ", DoubleToString( marginRequirement, 2 ), ", \n" ); 
      StringConcatenate( displayStr, displayStr, "  Current Spread: ", IntegerToString( currentSpread ), " of ", MaxSpread, ", \n" );
      StringConcatenate( displayStr, displayStr, "  Avg. Spread: ", DoubleToString( avgSpread/Point() ,0 ), "\n" );
      StringConcatenate( displayStr, displayStr, "  CommissionPoints: ", DoubleToString( commissionPoints / Point(), 0 ), " \n" ); 
      StringConcatenate( displayStr, displayStr, "  Broker StopLevel: ", IntegerToString(stoplevel), " \n" ); 
      
      StringConcatenate( displayStr, displayStr, "  GMT Now: ", niceHour(gmt)," \n" ); 
      StringConcatenate( displayStr, displayStr, " ----------------------------------------", "\n" );
      StringConcatenate( displayStr, displayStr, "  Set: ", TradeComment, " \n" ); 
      StringConcatenate( displayStr, displayStr, " ----------------------------------------", "\n" );         
      StringConcatenate( displayStr, displayStr, "  Velocity: ", DoubleToString( rateChange / Point(), 0 ), " \n" ); 
      StringConcatenate( displayStr, displayStr, "  Volatility: ", DoubleToString( volatility,0 ), "\n" );
      StringConcatenate( displayStr, displayStr, "  Acceleration: ", DoubleToString( avgAcceleration,5 ), "\n" );
      StringConcatenate( displayStr, displayStr, "  Acceleration   UP: ", DoubleToString( accelerationUp,5 ), "\n" );
      StringConcatenate( displayStr, displayStr, "  Acceleration DOWN: ", DoubleToString( accelerationDown,5 ), "\n" );
      

      Comment( displayStr ); 
  }
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   Print("Max Velocity=",IntegerToString(velocityRecord)," Max Acceleration=",DoubleToString(accelerationRecord,5));
  }