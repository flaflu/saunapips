//+------------------------------------------------------------------+
//|                                                         FFFD.mq4 |
//|                                                Flamínio Maranhão |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "The Sauna Team"
#property link      ""
#property version   "0.04"
#property strict

/*
 * Reversor EA - Alpha Release
 * 
 *                              !!! WARNING !!!
 * 
 *                  * * * THIS IS A WORK IN PROGRESS * * *
 *
 * Please note this is a proof of concept of an specific trade strategy
 * known as "Close Outside Close Inside" (the bollinger bands), designed to
 * antecipate trend reversions. It's a free implementation of the strategy 
 * described in the following link:
 *
 * http://atpalex.blogspot.com/2015/08/setup-fechou-fora-fechou-dentro.html 
 *
 * Future versions will include other reversion setups and better stop loss 
 * management
 * 
 * São Paulo / Brazil
 * 2019-02-23
 */

//--- input parameters
input string   EA_Description="THIS_IS_A_TEST"; 
input int      MagicNumber=9371831;             
input double   MaxRisk=6.0;                     
input double   PartialRealization=12.5;         
input double   TrailingDistance=360;            
input string   Technical_Boring_Stuff="=====================================";
input int      BOLLINGER_PERIOD=20;
input int      BOLLINGER_DEVIATION=2;
input int      SLOW_MOVING_AVERAGE_PERIOD=24;
input int      FAST_MOVING_AVERAGE_PERIOD=8;
input int      RSI_PERIOD=14;
input int      RSI_OVERBOUGHT_LEVEL=75;
input int      RSI_OVERSOLD_LEVEL=25;
input bool     DEUBUG=false;

//--- Internal Variables
int      MAX_CANDLES_TO_WAIT=2;              // Setup condition, see docs
int      TP_DISTANCE=1200;                   // Never used - Only to leave in the trade in case the EA is disconnected
string   WORKING_DIR="FFFD_WorkingTrades";   // Folder used by the EA to store open trades' states

enum Stage {
   PLACED = 0,    // Stop order is placed
   STAGE_1 = 1,   // Order was opened
   STAGE_2 = 2,   // Bollinger's middle bands were reached
   STAGE_3 = 3,   // Trend reversion was confirmed by moving averages
   CLOSED = 99,   // Closed
};   

enum TradeSignal {
   NONE = 0, // No signal
   SELL = 1, // SELL Signal
   BUY  = 2, // BUY Signal
};

int Slippage=3;   // @todo: Something better

// Constants - Broker's particular settings
double MIN_POINTS;   // Min points between current price and target prices
double MIN_LOTS;     // Min lots allowed in each trade
double MAX_LOTS;     // Max lots allowed in each trade


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {
   EventSetTimer(60);   // Not user yet (doesn's work in BackTest...)
   
   SetupBrokerGlobals();
   return(INIT_SUCCEEDED);
}

void SetupBrokerGlobals() {
   MIN_POINTS = MarketInfo(Symbol(), MODE_STOPLEVEL)+Slippage;
   MIN_LOTS   = MarketInfo(Symbol(), MODE_MINLOT);
   MAX_LOTS   = MarketInfo(Symbol(), MODE_MAXLOT);
}

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
   EventKillTimer();
}

void OnTimer() { 
   // Doesn't work in backtest;
   // routine "IsNewCandle" used instead
   //HandleTrade(NONE); 
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+


// Main Loop
void OnTick() {
   if (isNewBar()) {
      // Please Note: This is designed to be multisymbol in the future
      // Symbol is always treat as parameter, not as a global.
      onCandleClosed(Symbol());
   }
   updateDisplay();
}

void onCandleClosed(string symbol) {

   // First we check if there is some good signal for this symbol
   TradeSignal signal = GetSignal(symbol, Period());
   
   // Then we manage the open trades.
   HandleTrade(symbol, signal);
   
   // We finnaly check for new setup to be made
   switch (signal) {
      case SELL:
         SetupSell(symbol);
         break;
      case BUY:
         SetupBuy(symbol);
         break;
      case NONE:
         break;
   }
}

//+------------------------------------------------------------------+
//| Main EA functions                                                |
//+------------------------------------------------------------------+


// Check if a BUY or SELL setup was made in last 2 candles
TradeSignal GetSignal(string symbol, int timeframe) {

   // Bollinger bands
   double topBorder2 = iBands(symbol, timeframe, BOLLINGER_PERIOD, BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_UPPER, 2);
   double topBorder1 = iBands(symbol, timeframe, BOLLINGER_PERIOD, BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_UPPER, 1);
   double bottomBorder2 = iBands(symbol, timeframe, BOLLINGER_PERIOD, BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_LOWER, 2);
   double bottomBorder1 = iBands(symbol, timeframe, BOLLINGER_PERIOD, BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_LOWER, 1);
   double bma1 = iBands(symbol, timeframe, BOLLINGER_PERIOD, BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_MAIN, 1);
   
   // Candle info
   double close2 = iClose(symbol, timeframe, 2); 
   double close1 = iClose(symbol, timeframe, 1);
   double high1 = iHigh(symbol, timeframe, 1);
   double low1 = iLow(symbol, timeframe, 1);
   
   // Volumes
   long volume2 = iVolume(symbol, timeframe, 2);
   long volume1 = iVolume(symbol, timeframe, 1);
   
   bool volumeIncreasing = volume1 > volume2;

   // SMA
   double slowSMA2 = iMA(symbol, timeframe, SLOW_MOVING_AVERAGE_PERIOD, 0, MODE_SMMA, PRICE_CLOSE, 2);
   double fastSMA2 = iMA(symbol, timeframe, FAST_MOVING_AVERAGE_PERIOD, 0, MODE_SMMA, PRICE_CLOSE, 2);
   double slowSMA1 = iMA(symbol, timeframe, SLOW_MOVING_AVERAGE_PERIOD, 0, MODE_SMMA, PRICE_CLOSE, 1);
   double fastSMA1 = iMA(symbol, timeframe, FAST_MOVING_AVERAGE_PERIOD, 0, MODE_SMMA, PRICE_CLOSE, 1);
   
   bool SMAconvergence = MathAbs(slowSMA2-fastSMA2) > MathAbs(slowSMA1-fastSMA1);

   // The final criteria is:
   // Close outside, Close inside
   // Last candle din't reach the central band
   // Volume is increasing
   // Moving averages are in inverse trend of the signal, but converging

   if (close2 > topBorder2 && close1 < topBorder1 && low1 > bma1 && volumeIncreasing) {
      if (SMAconvergence && low1 > slowSMA1 && fastSMA1 > slowSMA1) {
         return SELL;
      } ;  
   }
   if (close2 < bottomBorder2 && close1 > bottomBorder1 && high1 < bma1 && volumeIncreasing) {
      if (SMAconvergence && high1 < slowSMA1 && fastSMA1 < slowSMA1) {
         return BUY;
      };   
   }
   return NONE;
}

// Carrying stragegy (In fact this can be applyed to any reversion strategy)
void HandleTrade(string symbol, TradeSignal signal) {

   bool found = False;
   
   // Basic routine to find this EA's trades
   for(int pos = 0; pos < OrdersTotal(); pos++) {
      bool o = OrderSelect(pos, SELECT_BY_POS, MODE_TRADES);
      if(OrderSymbol() != symbol) {
         continue;
      }
      if(OrderMagicNumber() == MagicNumber) {
         // found
         found = True;

         // Gather market data & input data
         double partialLots = NormalizeLots(PartialRealization*OrderLots()/100.0);
         double slowSMA = iMA(NULL, NULL, SLOW_MOVING_AVERAGE_PERIOD, 0, MODE_SMMA, PRICE_CLOSE, 0);
         double fastSMA = iMA(NULL, NULL, FAST_MOVING_AVERAGE_PERIOD, 0, MODE_SMMA, PRICE_CLOSE, 0);

         double topBorder =    iBands(symbol, Period(), BOLLINGER_PERIOD, BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_UPPER, 1);
         double bma =          iBands(symbol, Period(), BOLLINGER_PERIOD, BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_MAIN, 1);
         double bottomBorder = iBands(symbol, Period(), BOLLINGER_PERIOD, BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_LOWER, 1);
         double lastHigh =     iHigh (symbol, Period(), 1);
         double lastLow =      iLow  (symbol, Period(), 1);
         double lastOpen =     iOpen (symbol, Period(), 1);
         double lastClose =    iClose(symbol, Period(), 1);
         double rsi =          iRSI  (symbol, Period(), RSI_PERIOD, PRICE_CLOSE, 1);         
         
         bool ret;
         
         // -----------------------------
         // 1. Total Closing
         // 1.1 Reversion signal routines 
         if (OrderType() == OP_BUY && signal == SELL) {
            Debug(StringConcatenate("Closing by 1.1 (buy)"));         
            ret = OrderClose(OrderTicket(), OrderLots(), Bid, Slippage, clrGreen);
            FinalizeTrade(symbol);
            break;
         }
         if (OrderType() == OP_SELL && signal == BUY) {
            Debug(StringConcatenate("Closing by 1.1 (sell)"));         
            ret = OrderClose(OrderTicket(), OrderLots(), Ask, Slippage, clrGreen);
            FinalizeTrade(symbol);
            break;
         }
         
         // 1.2 Trend colding routines
         if (OrderType() == OP_BUY && lastClose > topBorder && lastClose <= lastOpen && rsi >= RSI_OVERBOUGHT_LEVEL) {
            Debug(StringConcatenate("Closing by 1.2 (buy)"));         
            ret = OrderClose(OrderTicket(), OrderLots(), Bid, Slippage, clrGreen);
            FinalizeTrade(symbol);
            break;
         }
         
         if (OrderType() == OP_SELL && lastClose < bottomBorder && lastClose >= lastOpen && rsi <= RSI_OVERSOLD_LEVEL) {
            Debug(StringConcatenate("Closing by 1.2 (sell)"));         
            ret = OrderClose(OrderTicket(), OrderLots(), Ask, Slippage, clrGreen);
            FinalizeTrade(symbol);
            break;
         }
         
         // 2. StopLoss Management
         
         // 2.1 (aux) Determine trade stage
         Stage tradeStage = GetTradeStage(symbol); // Disk I/O
         if (tradeStage == PLACED) {
            tradeStage = STAGE_1;
         }
                  
         // 2.2 Stop Loss Analysis
         double nextStopLoss   = OrderStopLoss();  // Updated stop loss
         double nextTakeProfit = OrderTakeProfit();// Updated take profit
         int    partialCounter = 0;                // How many "partials" closing will be made (could became a "total")
         
         if (tradeStage == STAGE_1) {
            // middle band reached. Could be a profit or a loss, and in case of loss we just quit.
            if (OrderType() == OP_BUY && lastClose > bma) {
               tradeStage = STAGE_2;
               if (Bid > OrderOpenPrice()) {
                  nextStopLoss   = NormalizeDouble(MathMin(OrderOpenPrice(), Bid - MIN_POINTS * Point), Digits);
                  nextTakeProfit = NormalizeDouble(Bid + TP_DISTANCE*Point, Digits);
                  partialCounter = partialCounter + 1;
               }
               else {
                  // @todo: Maybe just a partial closing and insist on trade? Parametrize it?
                  Debug(StringConcatenate("Closing by 2.2 (buy)"));
                  ret = OrderClose(OrderTicket(), OrderLots(), Bid, Slippage, clrBlack);
                  FinalizeTrade(symbol);
                  return;
               }
            }
            if (OrderType() == OP_SELL && lastClose < bma) {
               tradeStage = STAGE_2; 
               if (Ask < OrderOpenPrice()) {
                  nextStopLoss   = NormalizeDouble(MathMax(OrderOpenPrice(), Ask + MIN_POINTS * Point), Digits);
                  nextTakeProfit = NormalizeDouble(Ask - TP_DISTANCE*Point, Digits);
                  partialCounter = partialCounter + 1;
               }
               else {
                  Debug(StringConcatenate("Closing by 2.2 (sell)"));
                  ret = OrderClose(OrderTicket(), OrderLots(), Bid, Slippage, clrBlack);
                  FinalizeTrade(symbol);
                  return;
               }
            }
         }
         
         // We might go straight from stage 1 to 3, so no "else if" here.
         if (tradeStage == STAGE_2) {
            // Will set the stop loss to break-even price (or almost it - @todo make it so)
            if (OrderType() == OP_BUY) {            
               nextStopLoss   = MathMax(slowSMA-TrailingDistance*Point/2, OrderOpenPrice());
               nextStopLoss   = NormalizeDouble(MathMin(nextStopLoss, Bid - MIN_POINTS * Point), Digits);
               nextTakeProfit = NormalizeDouble(Bid + TP_DISTANCE*Point, Digits);
               if (fastSMA > slowSMA) {
                  // Trend is confirmed by Moving Averages Crossing
                  tradeStage = STAGE_3; 
                  partialCounter = partialCounter + 1;
               }
            }
            if (OrderType() == OP_SELL) {
               nextStopLoss   = MathMin(fastSMA+TrailingDistance*Point/2, OrderOpenPrice());
               nextStopLoss   = NormalizeDouble(MathMax(nextStopLoss, Ask + MIN_POINTS * Point), Digits);
               nextTakeProfit = NormalizeDouble(Ask - TP_DISTANCE*Point, Digits);
               if(fastSMA < slowSMA) {
                  tradeStage = STAGE_3;
                  partialCounter = partialCounter + 1;
               }
            }
         }
         
         if (tradeStage == STAGE_3) {
            // Fun stuff: Trailing stop & more partial closings.
            
            // Trailing stop: will follow fastSMA + TrailingDistance/2
            if (OrderType() == OP_BUY && fastSMA > slowSMA){
               nextStopLoss = MathMax(fastSMA-TrailingDistance*Point/2, OrderOpenPrice());
               nextStopLoss = NormalizeDouble(MathMin(nextStopLoss, Bid - MIN_POINTS * Point), Digits);
            }
            if (OrderType() == OP_SELL && fastSMA < slowSMA) {
               nextStopLoss = MathMin(fastSMA+TrailingDistance*Point/2, OrderOpenPrice());
               nextStopLoss = NormalizeDouble(MathMax(nextStopLoss, Ask + MIN_POINTS * Point), Digits);
            }
            
            // A very good candle (open and closed outside the borders in favor of trade) will increase the stoploss
            if (OrderType() == OP_BUY && lastClose > lastOpen && lastOpen > topBorder) {
               nextStopLoss = lastLow-TrailingDistance*Point/2;
               nextStopLoss = NormalizeDouble(MathMin(nextStopLoss, Bid - MIN_POINTS * Point), Digits);
            }
            if (OrderType() == OP_SELL && lastClose < lastOpen && lastOpen < bottomBorder) {
               nextStopLoss =lastHigh+TrailingDistance*Point/2;
               nextStopLoss = NormalizeDouble(MathMax(nextStopLoss, Ask + MIN_POINTS * Point), Digits);
            }
         }
         
         // Check for SL Update 
         if ((OrderType() == OP_BUY && nextStopLoss > OrderStopLoss()) || (OrderType() == OP_SELL && nextStopLoss < OrderStopLoss())) {
            Debug(StringConcatenate("updating stoploss to ", nextStopLoss));
            ret = OrderModify(OrderTicket(), OrderOpenPrice(), nextStopLoss, nextTakeProfit, 0, clrYellow);
         }
         
         // No more state changing can be made, let's persist it
         SetTradeStage(symbol, tradeStage); // Disk I/O
         
         // 3. Partial closing analysis

         // 3.1 RSI
         // RSI is in extreme value: close some positions
         if (OrderType() == OP_BUY && rsi > RSI_OVERBOUGHT_LEVEL) {
            partialCounter = partialCounter + 1;
         }
         if (OrderType() == OP_SELL && rsi < RSI_OVERSOLD_LEVEL) {
            partialCounter = partialCounter + 1;
         }
                  
         // 3.2 Open and close outside the bollinger borders
         // Open and close outside the border *against* the trade: close some positions
         if (OrderType() == OP_BUY && lastOpen > topBorder && lastClose > topBorder) {
            partialCounter = partialCounter + 1;
         }
         if (OrderType() == OP_SELL && lastOpen < bottomBorder && lastClose < bottomBorder) {
            partialCounter = partialCounter + 1;
         }
         
         
         // 4. Order management
         double totalLots = MathMin(partialCounter*partialLots, OrderLots()); // lets sum all "partials"
         if (OrderType() == OP_BUY && totalLots > 0) {
            Debug(StringConcatenate("Closing by 4.1 partialCounter=", partialCounter, ",totalLots=", totalLots));
            ret = OrderClose(OrderTicket(), NormalizeLots(totalLots), Bid, Slippage, clrLightGreen);
         }
         if (OrderType() == OP_SELL && totalLots > 0) {
            Debug(StringConcatenate("Closing by 4.1 partialCounter=", partialCounter, ",totalLots=", totalLots));
            ret = OrderClose(OrderTicket(), NormalizeLots(totalLots), Ask, Slippage, clrLightGreen);
         
         }
         if (totalLots >= OrderLots()) {
            FinalizeTrade(symbol);
         }
      } // end: if(OrderMagicNumber() == MagicNumber)
   } // end:  for(int pos = 0; pos < OrdersTotal(); pos++)
   
   if (found == False) {
      // No traddes open by this EA
   }
}

//+------------------------------------------------------------------+
//| Auxiliary File Handling Routines                                 |
//+------------------------------------------------------------------+
// https://docs.mql4.com/files
// @todo: All fallback routines in case we couldn't retrieve something
// @todo: Trades closed by TP must have their files deleted

// Premisse here: Only one trade by symbol/timeframe can be handled
string _MakeFileName(string symbol) {
   return WORKING_DIR+"//"+symbol+"_"+IntegerToString(Period(), 5, '0')+".txt";
}

Stage GetTradeStage(string symbol) {
   int ifHandle=FileOpen(_MakeFileName(symbol),FILE_READ|FILE_BIN);
   if (ifHandle != INVALID_HANDLE) {
      Stage tradeStage = (Stage)FileReadInteger(ifHandle);
      FileClose(ifHandle);
      Debug(StringConcatenate("[INFO] Read: ", _MakeFileName(symbol), " value was: ", tradeStage));
      return tradeStage;
   } else {
      Print(StringConcatenate("[ERROR] Could not open for reading: ", _MakeFileName(symbol)));
      return CLOSED;
   }
}

void SetTradeStage(string symbol, Stage tradeStage) {
   int ofHandle=FileOpen(_MakeFileName(symbol),FILE_WRITE|FILE_BIN);
   if (ofHandle != INVALID_HANDLE) {
      FileWriteInteger(ofHandle, (int)tradeStage);
      FileClose(ofHandle);
      Debug(StringConcatenate("[INFO] Set: ", _MakeFileName(symbol), " to: ", tradeStage));
   } else {
      Print(StringConcatenate("[ERROR] Could not open to write: ", _MakeFileName(symbol), " value was: ", tradeStage));
   }
}

void FinalizeTrade(string symbol) {
   bool delOK = FileDelete(_MakeFileName(symbol));
   if (delOK == True) {
      Print(StringConcatenate("[INFO] Delete: ", _MakeFileName(symbol)));
   } else {
      Print(StringConcatenate("[ERROR] Could not delete file: ", _MakeFileName(symbol)));
   };
}


//+------------------------------------------------------------------+
//| Auxiliary Routines                                               |
//+------------------------------------------------------------------+

// Check if we are in a new bar - copyed from somewhere around
bool isNewBar() {
    static datetime lastbar;
    datetime curbar = Time[0];
    if(lastbar!=curbar) {
        lastbar=curbar;
        return true;
    }
    else {
        return false;
    }
};

void updateDisplay() {
   //string display = StringConcatenate("Fecha Fora Fecha Dentro 0.03\n");
   //display = StringConcatenate( display, "I'll try to put some useful information here.\n" );
   //Comment(display);
};

// Normalizes the number of Lots, considering max an min lots
double NormalizeLots(double lots) {
   // @todo: In case we want to open more lots than "max_lots", we should open
   // more trades.
   return NormalizeDouble(MathMin(MathMax(lots, MIN_LOTS), MAX_LOTS), Digits);
}


// Gets the number of lots to trade, given the StopLoss evaluated in trade setup
double EvalLots(double stopLossInPoints) {
   double capitalToUse = MathMin(AccountBalance(), AccountEquity()); // safer, but maybe could be parametrized
   double lots = NormalizeLots((MaxRisk/100)*capitalToUse/stopLossInPoints);
   return lots;
}

// Helper to place orders
int _SetupTrade(string symbol, int opType, double tradePrice, double stopLossInPoints, double takeProfitInPoints) {
   double baseStopLossValue   = MathMax(MIN_POINTS, stopLossInPoints  )+TrailingDistance;
   double baseTakeProfitValue = MathMax(MIN_POINTS, takeProfitInPoints);
   double tradeLots           = EvalLots(baseStopLossValue);
   datetime expiration        = TimeCurrent() + MAX_CANDLES_TO_WAIT *Period()*60+ 1;
   double takeProfitPrice     = 0.0;
   double stopLossPrice       = 0.0;
   color tradeColor           = clrBlack;
   
   if (opType == OP_SELL || opType == OP_SELLLIMIT || opType == OP_SELLSTOP) {
      tradePrice      = NormalizeDouble(MathMin(tradePrice, tradePrice-MIN_POINTS*Point), Digits);
      stopLossPrice   = NormalizeDouble(tradePrice + baseStopLossValue*Point, Digits);
      takeProfitPrice = NormalizeDouble(tradePrice - takeProfitInPoints*Point, Digits);
      tradeColor      = clrRed;
   } 
   else if (opType == OP_BUY || opType == OP_BUYLIMIT || opType == OP_BUYSTOP) {
      tradePrice      = NormalizeDouble(MathMax(tradePrice, tradePrice+MIN_POINTS*Point), Digits);
      stopLossPrice   = NormalizeDouble(tradePrice - baseStopLossValue*Point, Digits);
      takeProfitPrice = NormalizeDouble(tradePrice + takeProfitInPoints*Point, Digits);   
      tradeColor      = clrBlue;
   }
   else {
      // Bizarre opType
      return 0;
   }
   int ticket = OrderSend(symbol, opType, tradeLots, tradePrice, Slippage, stopLossPrice, takeProfitPrice, EA_Description, MagicNumber, expiration, tradeColor);
   if (ticket < 0) {
      Print(StringConcatenate("[ERROR] Order error;symbol;", symbol,";opType;",opType, ";tradeLots;", tradeLots, ";tradePrice;", tradePrice, ";Slippage;", Slippage, ";stopLossPrice;", stopLossPrice, ";takeProfitPrice;", takeProfitPrice, ";expiration;", expiration));
   }
   SetTradeStage(symbol, PLACED);
   return ticket;
};

// Evaluate and set up a sell order.
void SetupSell(const string symbol) {
   double baseStopLossPrice   = iHigh (symbol, Period(), 1);
   double baseEnterPrice      = iLow  (symbol, Period(), 1);
   double baseTakeProfitPrice = iBands(symbol, Period(), BOLLINGER_PERIOD, BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_LOWER, 1);
   double stopLossInPoints    = NormalizeDouble((baseStopLossPrice-baseEnterPrice)/Point, Digits);
   double takeProfitInPoints  = NormalizeDouble((baseEnterPrice-baseTakeProfitPrice)/Point, Digits);
   _SetupTrade(symbol, OP_SELLSTOP, baseEnterPrice, stopLossInPoints, takeProfitInPoints);
};

// Evaluate and set up a buy order.
void SetupBuy(const string symbol) {
   double baseStopLossPrice   = iLow  (symbol, Period(), 1);
   double baseEnterPrice      = iHigh (symbol, Period(), 1);
   double baseTakeProfitPrice = iBands(symbol, Period(), BOLLINGER_PERIOD, BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_UPPER, 1);
   double stopLossInPoints    = NormalizeDouble((baseEnterPrice-baseStopLossPrice)/Point, Digits); 
   double takeProfitInPoints  = NormalizeDouble((baseTakeProfitPrice-baseEnterPrice)/Point, Digits);
   _SetupTrade(symbol, OP_BUYSTOP, baseEnterPrice, stopLossInPoints, takeProfitInPoints);
};

void Debug(string text) {
   if (DEUBUG) {
      Print(text);
   }
};      