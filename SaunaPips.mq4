#property copyright "Copyright SaunaForex"
#property link "https://saunaforex.000webhostapp.com" 
#property strict
#property version "1.03a"

enum intOptionsTrade
{
   A1 = 1,//in favor
   A2 = 2,//against
   A3 = 3,//both
};

extern string TradeComment = "SaunaPips v1.03a"; 
extern int MagicNumber = 171717;
extern int Slippage = 3;
extern int MaxSpread = 18;
extern double FixedLot = 0;
extern int RiskPercent = 30;
extern int StartHour = 0;
extern int EndHour = 23;
extern int GMTOffset = 0;

extern int maxBuyStopOrders = 1;
extern int maxSellStopOrders = 1;

//Aqui o Velocity trabalha com Range, a regra e assim:
//Se o rateChange (ou Velocity) for maior que VelocityRange - VelocityStop (90-20=70) entao gatilha pra disparar uma Stop Order
//O que define a direcao da ordem eh a forca do rateChange, 
//Se estiver no range inferior, nesse exemplo, entre 70 e 90, ele opera contra a tendencia
//Se estiver no range superior, nesse exemplo, maior ou igual a 90, ele opera a favor da tendencia
extern int VelocityRange = 90; 
extern int VelocityStop = 20; 

//Tempo em segundos para apagar Stop Order pendentes que nao foram acionadas e para atualizar o valor do rateChange
extern int VelocityTime = 7; 

//Distancia em pontos/pippets que vai abrir a Stop Order do preco atual
extern int Velocity_StopOrderPoints = 10; 

//Distancia em pontos/pippets que o StopLoss vai ficar do preco apos a abertura da ordem
extern int Velocity_StopLossTrail = 20; 

//Distancia em pontos/pippets maxima que o Trail do SL vai andar a favor do trade (lucro)
extern int Velocity_TrailHold = 40; 

//Tempo em minutos que o trade vai aguardar pra poder ultrapassar o Trail do SL alem do Velocity_TrailHold
extern int Velocity_TrailResume = 9;

extern intOptionsTrade TradeTendency = A3;

//Liga ou desliga o StopLoss controlado internamente pelo EA
extern bool useDynamicStopLoss = 1;

// Stop Loss com Spread
extern bool useSpreadStopLoss = 1;

extern bool DebugMode = 0;

//Esse TickSample parece ser bem importante, ele define qual sera o tamanho da amostragem para calcular varias medias, como por exemplo a media do spread
//Talvez seja interessante deixar ele externo como um parametro dos Inputs do EA, com isso poderia otimizar o valor ao inves de ficar sempre igual a 100
int TickSample = 100;

int r, gmt, brokerOffset, size, digits, stoplevel;

double marginRequirement, maxLot, minLot, lotSize, points, currentSpread, avgSpread, maxSpread, initialBalance, rateChange, commissionPoints;

double spreadSize[]; 
double tick[];
double avgtick[];
int tickTime[];

//Arrays utilizados para controle do Dynamic StopLoss
int dynamicSLTicket[];
double dynamicSL[];

string testerStartDate, testerEndDate; 

int lastBuyOrder, lastSellOrder, totalBuyStop, totalSellStop, totalBuy, totalSell;

bool calculateCommission = true;

int init() {
   marginRequirement = MarketInfo( Symbol(), MODE_MARGINREQUIRED ) * 0.01;
   maxLot = ( double ) MarketInfo( Symbol(), MODE_MAXLOT );
   minLot = ( double ) MarketInfo( Symbol(), MODE_MINLOT );
   currentSpread = NormalizeDouble( Ask - Bid, Digits );
   stoplevel = ( int ) MathMax( MarketInfo( Symbol(), MODE_FREEZELEVEL ), MarketInfo( Symbol(), MODE_STOPLEVEL ) );
   if( stoplevel > Velocity_StopOrderPoints ) Velocity_StopOrderPoints = stoplevel;
   if( stoplevel > Velocity_StopLossTrail ) Velocity_StopLossTrail = stoplevel;
   avgSpread = currentSpread;
   size = TickSample;
   ArrayResize( spreadSize, size ); 
   ArrayFill( spreadSize, 0, size, avgSpread );
   maxSpread = NormalizeDouble( MaxSpread * Point, Digits );
   testerStartDate = StringConcatenate( Year(), "-", Month(), "-", Day() );
   initialBalance = AccountBalance();   
    
   if(useDynamicStopLoss)
      initDynamicSL();   
   
   display();
   return ( 0 );
}


//Essa funcao serve para reajustar todos StopLoss dinamicos caso o EA tenha sido reiniciado com trades abertos
void initDynamicSL(){
   ArrayResize(dynamicSL, OrdersTotal());
   ArrayResize(dynamicSLTicket, OrdersTotal());
   for( int pos = 0; pos < OrdersTotal(); pos++ ) {
      r = OrderSelect( pos, SELECT_BY_POS, MODE_TRADES );
      if( OrderSymbol() != Symbol() ) continue;
      if( OrderMagicNumber() == MagicNumber ){
         dynamicSLTicket[pos] = OrderTicket();
         switch ( OrderType() ) {
            case OP_BUY:            
               dynamicSL[pos] = setStopLossBUY();
            break;
            case OP_SELL:
               dynamicSL[pos] = setStopLossSELL();
            break;      
         }
      }
   }      
}

// Se tiver um jeito de passar o ORDER_TYPE como parâmetro aqui, dá pra juntar as 2 funções
// ref: http://www.coensio.com/wp/spread-stop-loss-and-take-profit/
double setStopLossBUY(){
   if (useSpreadStopLoss) {
      double Spread = Ask - Bid;
      return NormalizeDouble(Bid + Spread - (Velocity_StopLossTrail * Point), Digits);
   } else {
      return NormalizeDouble(Bid - (Velocity_StopLossTrail * Point), Digits);   
   }   
}

double setStopLossSELL(){
   if (useSpreadStopLoss) {
      double Spread = Ask - Bid;
      // É BID mesmo, não é ASK
      return NormalizeDouble(Bid - (Velocity_StopLossTrail * Point), Digits);
   } else {
      return NormalizeDouble(Ask - (Velocity_StopLossTrail * Point), Digits);
   }   
}


//Calcula a comissao analisando o historico de trades, executa so uma vez
//Teoricamente, se for uma conta zerada, o primeiro trade do EA vai ser feito sem levar em conta a comissao
void commission(){
   double rate = 0;
   for( int pos = OrdersHistoryTotal() - 1; pos >= 0; pos-- ) {
      if( OrderSelect( pos, SELECT_BY_POS, MODE_HISTORY ) ) {
         if( OrderProfit() != 0.0 ) {
            if( OrderClosePrice() != OrderOpenPrice() ) {
               if( OrderSymbol() == Symbol() ) {
                  calculateCommission = false;
                  rate = MathAbs( OrderProfit() / MathAbs( OrderClosePrice() - OrderOpenPrice() ) );
                  commissionPoints = ( -OrderCommission() ) / rate;
                  break;
               }
            }
         }
      }
   }   
}

//Funcao so pra retornar o horario "local" apos ajuste do Fuso horario
int offlineGMT(){
   int bkrH = Hour();
   int gOffset =  bkrH - GMTOffset; 
   if( gOffset < 0 ) gOffset += 24;
   else if( gOffset > 23 ) gOffset -= 24;
   return ( gOffset );
}

int start() {   
   totalBuyStop = 0;
   totalSellStop = 0;
   totalBuy = 0;
   totalSell = 0;
   
   int ticket;   
   double maxStopLoss;
   double newStopLoss;
   double realStopLoss;
   
   double accountEquity = AccountBalance() + OrderProfit() + OrderCommission() + OrderSwap();;

   if( calculateCommission ) commission();
   gmt = offlineGMT();
   prepareSpread(); //Atualiza a media do spread (avgSpread)
   manageTicks();  //Atualiza o valor da oscilacao (rateChange)  
   
   //Fecha as ordens manualmente se o DynamicSL estiver setado
   if(useDynamicStopLoss)
      checkDynamicSL();
   
   //Aqui ele faz um loop em todas posicoes pendentes e abertas, conforme: 
   // As pendentes: apaga as que nao foram acionadas dentro do VelocityTime e ajusta o trail Stop Order
   // As abertas:  ajusta o trailStop
   for( int pos = 0; pos < OrdersTotal(); pos++ ) {
      r = OrderSelect( pos, SELECT_BY_POS, MODE_TRADES );
      if( OrderSymbol() != Symbol() ) continue;
      if( OrderMagicNumber() == MagicNumber ){
         switch ( OrderType() ) {
            case OP_BUYSTOP:
               if( (int) TimeCurrent() - lastBuyOrder > VelocityTime ){
                  r = OrderDelete( OrderTicket() );
               }else if( OrderOpenPrice() - Ask > Velocity_StopOrderPoints * Point){
                  r = OrderModify( OrderTicket(), NormalizeDouble( Ask + ( Velocity_StopOrderPoints * Point ), Digits ), OrderStopLoss(), OrderTakeProfit(), 0 );
               }
               totalBuyStop++;
            break;
            case OP_SELLSTOP:
               if( (int) TimeCurrent() - lastSellOrder > VelocityTime ){
                  r = OrderDelete( OrderTicket() );
               } else if( Bid - OrderOpenPrice() > Velocity_StopOrderPoints * Point) {
                  r = OrderModify( OrderTicket(), NormalizeDouble( Bid - ( Velocity_StopOrderPoints * Point ), Digits ), OrderStopLoss(), OrderTakeProfit(), 0 );
               }
               totalSellStop++;
            break;
            case OP_BUY:
               
               realStopLoss = OrderStopLoss();
               if(useDynamicStopLoss){
                  int slPos = getRealStopLossPos(pos, OrderTicket());
                  if(slPos >= 0)
                     realStopLoss = dynamicSL[slPos];
               }
               
               if(realStopLoss == 0.0 || Bid - realStopLoss > Velocity_StopLossTrail * Point ){
                  
                  //Se a idade em segundos da ordem for menor que o Velocity_TrailResume
                  if( (int)TimeCurrent() - (int)OrderOpenTime() < Velocity_TrailResume*60){
                     
                     maxStopLoss = OrderOpenPrice() + (Velocity_TrailHold  * Point);
                     newStopLoss = setStopLossBUY();
                     
                     //Atualiza para o newStopLoss somente se estiver dentro do limite do Velocity_TrailHold
                     if(newStopLoss != realStopLoss && newStopLoss <= maxStopLoss){
                        r = updateStopLoss( pos, OrderTicket(), OrderOpenPrice(), NormalizeDouble(newStopLoss, Digits), OrderTakeProfit(), 0 );
                     }
                  }else{
                     //Ja passou o TrailResumo, pode ir ajustando o SL da ordem alem do TrailHold
                     r = updateStopLoss( pos, OrderTicket(), OrderOpenPrice(), NormalizeDouble(Bid - (Velocity_StopLossTrail * Point), Digits), OrderTakeProfit(), 0 );
                  }
               }
               totalBuy++;
            break;
            case OP_SELL:
               
               realStopLoss = OrderStopLoss();
               if(useDynamicStopLoss){
                  int slPos = getRealStopLossPos(pos, OrderTicket());
                  if(slPos >= 0)
                     realStopLoss = dynamicSL[slPos];
               }
               
               if(realStopLoss == 0.0 || realStopLoss - Ask >  Velocity_StopLossTrail * Point){
               
                  //Se a idade em segundos da ordem for menor que o Velocity_TrailResume
                  if( (int)TimeCurrent() - (int)OrderOpenTime() < Velocity_TrailResume*60){
                     
                     maxStopLoss = OrderOpenPrice() - (Velocity_TrailHold  * Point);
                     newStopLoss = setStopLossSELL();
                     
                     //Atualiza para o newStopLoss somente se estiver dentro do limite do Velocity_TrailHold
                     if(newStopLoss != realStopLoss && newStopLoss >= maxStopLoss){
                        r = updateStopLoss( pos, OrderTicket(), OrderOpenPrice(), NormalizeDouble(newStopLoss, Digits), OrderTakeProfit(), 0 );
                     }
                  }else{
                     //Ja passou o TrailResumo, pode ir ajustando o SL da ordem alem do TrailHold
                     r = updateStopLoss( pos, OrderTicket(), OrderOpenPrice(), NormalizeDouble(Ask + (Velocity_StopLossTrail * Point), Digits), OrderTakeProfit(), 0 );
                  }
               }
               totalSell++;
            break;
         }
      }
   }
   
   //Aqui que ele abre as novas posicoes conforme as condicoes abaixo
   if( ( ( StartHour < EndHour && gmt >= StartHour && gmt <= EndHour ) 
   || ( StartHour > EndHour && ( ( gmt <= EndHour && gmt >= 0 ) 
   || ( gmt <= 23 && gmt >= StartHour ) ) ) ) 
   ){
      if(avgSpread <= maxSpread){
         
         if(MathAbs(rateChange) > MathAbs(VelocityRange - VelocityStop)* Point){ //Se atingir o TriggerVelocity Ex: ( 90 - 20 = 70)
            
            bool tradeInFavor = (TradeTendency == 1 || (TradeTendency == 3 && MathAbs(rateChange) > VelocityRange * Point));
            bool triggerBuy = false;
            bool triggerSell = false;
            
            //Se for volatilidade Bulish
            if(rateChange > 0){
               if(tradeInFavor){
                  triggerBuy = true; //Segue Tendencia Bullish
               }else{
                  triggerSell = true; //ContraTendencia Bullish
               }
            }else if(rateChange < 0){ //Se for volatilidade Bearish
               if(tradeInFavor){
                  triggerSell = true; //Segue Tendencia Bearish
               }else{
                  triggerBuy = true; //ContraTendencia Bearish
               }
            }
            
            if(triggerBuy && totalBuyStop < maxBuyStopOrders)
               ticket = buy(totalBuyStop); 
            
            if(triggerSell && totalSellStop < maxSellStopOrders)
               ticket = sell(totalSellStop);
            
         }
      }
   }
    
   display(); 
   return ( 0 );
}

int updateStopLoss(int pos, int ticket, double openPrice, double newStopLoss, double takeProfit, datetime expiration){
   if(useDynamicStopLoss){
      //Pra alterar o SL dinamico, utiliza o POS no array e nao o TicketOrder
      int realPos = getRealStopLossPos(pos,ticket);
      if(realPos >= 0){
         dynamicSL[realPos] = newStopLoss;
      }else{
         //StopLoss desse Ticket nao existe, bora criar
         int newSize = ArraySize(dynamicSLTicket)+1;
         ArrayResize(dynamicSLTicket, newSize);
         ArrayResize(dynamicSL, newSize);
         
         dynamicSLTicket[newSize-1] = ticket;
         dynamicSL[newSize-1] = newStopLoss;
      }
      return 0;
   }else{
      return OrderModify(ticket, openPrice, newStopLoss, takeProfit, expiration );
   }
}

int getRealStopLossPos(int pos, int ticket){

   //Primeiro garante que a ordem da lista dos trades abertos nao mudaram
   if(ArraySize(dynamicSLTicket) > pos){
      if(dynamicSLTicket[pos] == ticket){
         return pos;
      }
   }
   
   //Se o broker mudou a ordem dos trades, procura a danada na lista
   for( int i = 0; i < ArraySize(dynamicSLTicket); i++ ) {
      if(dynamicSLTicket[i] == ticket)
         return i;
   }
   //Se chegou aqui nao encontrou o SL dessa ordem
   return -1;
}

void removeDynamicSL(int ticket){
  
   int realPos = getRealStopLossPos(0, ticket);
   if(realPos >= 0){
      removeIndex_dynamicSL(realPos);
      removeIndex_dynamicSLTicket(realPos);
   }
}

//Aqui ele faz um loop em todas orders correntes de Buy e Sell e guarda os tickets daquelas que atingiram o StopLoss
//em seguida ele fecha de fato todas as que atingiram o StopLoss
//Sao loops separados para garantir que a ordem dos trades da lista nao seja baguncado enquanto compara o StopLoss dinamico
void checkDynamicSL(){
   int realPos;
   int closeBuyTickets[];
   int closeSellTickets[];
   
   for( int pos = 0; pos < OrdersTotal(); pos++ ) {
      r = OrderSelect( pos, SELECT_BY_POS, MODE_TRADES );
      if( OrderSymbol() != Symbol() ) continue;
      if( OrderMagicNumber() == MagicNumber ){
         switch ( OrderType() ) {
            case OP_BUY:
               realPos = getRealStopLossPos(pos, OrderTicket());
               if(realPos >= 0){
                  if(dynamicSL[realPos] >= Bid){
                     int sizeArr = ArraySize(closeBuyTickets);
                     ArrayResize( closeBuyTickets, sizeArr+1);
                     closeBuyTickets[sizeArr] = OrderTicket();
                  }
               }
            break;
            case OP_SELL:
               realPos = getRealStopLossPos(pos, OrderTicket());
               if(realPos >= 0){
                  if(dynamicSL[realPos] <= Ask){
                     int sizeArr = ArraySize(closeSellTickets);
                     ArrayResize( closeSellTickets, sizeArr+1);
                     closeSellTickets[sizeArr] = OrderTicket();
                  }
               }
            break;
         }
      }
   }
   
   //Fecha todos trades Buy que atingiram o StopLoss
   for( int i = 0; i < ArraySize(closeBuyTickets); i++ ) {
      if(OrderSelect( closeBuyTickets[i], SELECT_BY_TICKET, MODE_TRADES )){
         int ticket = OrderTicket();
         if(OrderClose( OrderTicket(), OrderLots(), Bid, Slippage)){
            removeDynamicSL(ticket);
         }
      }
   }
   
   //Fecha todos trades Sell que atingiram o StopLoss
   for( int i = 0; i < ArraySize(closeSellTickets); i++ ) {
      if(OrderSelect( closeSellTickets[i], SELECT_BY_TICKET, MODE_TRADES )){
         int ticket = OrderTicket();
         if(OrderClose( OrderTicket(), OrderLots(), Ask, Slippage)){         
            removeDynamicSL(ticket);
         }
      }   
   }
}

int buy(int buyStop){
   int ticket = OrderSend( Symbol(), OP_BUYSTOP, lotSize(), Ask + ( buyStop + 1.0 ) * ( Point * Velocity_StopOrderPoints ), Slippage, 0, 0, TradeComment, MagicNumber, 0 );
   lastBuyOrder = ( int ) TimeCurrent();
   return ticket;
}

int sell(int sellStop){
   int ticket = OrderSend(Symbol(), OP_SELLSTOP, lotSize(), Bid - ( sellStop + 1.0 ) * ( Point * Velocity_StopOrderPoints ), Slippage, 0, 0, TradeComment, MagicNumber, 0 );
   lastSellOrder = ( int ) TimeCurrent();
   return ticket;   
}

double lotSize(){  
   if( FixedLot > 0 ){
      lotSize = NormalizeDouble( FixedLot, 2 );
   } else {
      if( marginRequirement > 0 ) 
         lotSize = MathMax( MathMin( NormalizeDouble( ( AccountBalance() * ( ( double ) RiskPercent / 1000 ) * 0.01 / marginRequirement ), 2 ), maxLot ), minLot );    
   }  
   return ( NormalizeLots( lotSize ) ); 
}  

double NormalizeLots( double p ){
    double ls = MarketInfo( Symbol(), MODE_LOTSTEP );
    return( MathRound( p / ls ) * ls );
}


// Essa funcao atualiza o array avgSpread com o novo spread, removendo o mais antigo 
// e em seguida calcula a media linear-ponderada dos elementos desse array atualizado
void prepareSpread(){   
   double spreadSize_temp[];
   ArrayResize( spreadSize_temp, size - 1 );
   ArrayCopy( spreadSize_temp, spreadSize, 0, 1, size - 1 );
   ArrayResize( spreadSize_temp, size );
   spreadSize_temp[size-1] = NormalizeDouble( Ask - Bid, Digits );
   ArrayCopy( spreadSize, spreadSize_temp, 0, 0 );
   avgSpread = iMAOnArray( spreadSize, size, size, 0, MODE_LWMA, 0 );   
}       

//Essa funcao calcula o rateChange, ou seja, a diferenca de valor de venda (Bid) 
// entre o momento atual e o valor q estava conforme os X "segundos"? atras, 
// onde X = valor da variavel VelocityTime
// O rateChange eh utilizado para ativar o gatilho de Compra/Venda, quanto maior
// esse valor, maior a oscilacao do par nesse momento
void manageTicks(){    
   double tick_temp[], tickTime_temp[], avgtick_temp[];
   ArrayResize( tick_temp, size - 1 );
   ArrayResize( tickTime_temp, size - 1 );
   ArrayCopy( tick_temp, tick, 0, 1, size - 1 ); 
   ArrayCopy( tickTime_temp, tickTime, 0, 1, size - 1 ); 
   ArrayResize( tick_temp, size ); 
   ArrayResize( tickTime_temp, size );
   tick_temp[size-1] = Bid;
   tickTime_temp[size-1] = ( int ) TimeCurrent();
   ArrayCopy( tick, tick_temp, 0, 0 );    
   ArrayCopy( tickTime, tickTime_temp, 0, 0 ); 
   int timeNow = tickTime[size-1];
   double priceNow = tick[size-1];
   double priceThen = 0;   
   int period = 0;
   for( int i = size - 1; i >= 0; i-- ){ 
      period++;
      if( timeNow - tickTime[i] > VelocityTime ){
         priceThen = tick[i]; 
         break;
      }  
   }   
    
   rateChange = ( priceNow - priceThen );
   
   // Aqui parece ser uma trava de seguranca, se a ultima variacao de PONTOS for maior que 5000 (ou 500 pips)
   // ele zera o rateChange, carai, 500 pips em segundos? so soltando uma ogiva em NY kkkkk
   if( rateChange / Point > 5000 ) rateChange = 0;     
} 

//So serve pra formatar o horario no Display
string niceHour( int kgmt ){ 
   string m;
   if( kgmt > 12 ) {
      kgmt = kgmt - 12;
      m = "PM"; 
   } else {
      m = "AM";
      if( kgmt == 12 )
         m = "PM";
   }
   return ( StringConcatenate( kgmt, m ) );
}

void removeIndex_dynamicSL(int index){
   double TempArray[];   
   int mySize = ArraySize(dynamicSL);
   
   if(index==0 && mySize == 1){ //Remove o unico elemento existente  
      ArrayFree(dynamicSL);
            
   }else if(index == 0 && mySize > 1){ //Remove o primeiro elemento   
      ArrayResize(TempArray, (mySize-1) );
      ArrayCopy(TempArray, dynamicSL, 0, 1, (mySize-1));      
      ArrayFree(dynamicSL);
      ArrayResize(dynamicSL, (mySize-1));
      ArrayCopy(dynamicSL, TempArray, 0, 0);      
      
   }else if(index > 0 && index == mySize-1){ //Remove o ultimo elemento 
      ArrayResize(dynamicSL, mySize-1);
      
   }else{ //Remove um elemento do meio do Array   
      ArrayResize(TempArray,mySize-1);
      ArrayCopy(TempArray,dynamicSL,0,0,index);
      ArrayCopy(TempArray,dynamicSL,index,index+1);
      ArrayResize(dynamicSL, mySize-1);
      ArrayCopy(dynamicSL,TempArray,0,0);
   }
}

void removeIndex_dynamicSLTicket(int index){
   int TempArray[];   
   int mySize = ArraySize(dynamicSLTicket);
   
   if(index==0 && mySize == 1){ //Remove o unico elemento existente  
      ArrayFree(dynamicSLTicket);
            
   }else if(index == 0 && mySize > 1){ //Remove o primeiro elemento   
      ArrayResize(TempArray, (mySize-1) );
      ArrayCopy(TempArray, dynamicSLTicket, 0, 1, (mySize-1));      
      ArrayFree(dynamicSLTicket);
      ArrayResize(dynamicSLTicket, (mySize-1));
      ArrayCopy(dynamicSLTicket, TempArray, 0, 0);      
      
   }else if(index > 0 && index == mySize-1){ //Remove o ultimo elemento 
      ArrayResize(dynamicSLTicket, mySize-1);
      
   }else{ //Remove um elemento do meio do Array   
      ArrayResize(TempArray,mySize-1);
      ArrayCopy(TempArray,dynamicSLTicket,0,0,index);
      ArrayCopy(TempArray,dynamicSLTicket,index,index+1);
      ArrayResize(dynamicSLTicket, mySize-1);
      ArrayCopy(dynamicSLTicket,TempArray,0,0);
   }
}

void PrintArray(string prefix, int &myArray[]){
   string texto = prefix;
   for(int i=0; i < ArraySize(myArray); i++){
      texto = StringConcatenate(texto,", ", IntegerToString(myArray[i]));
   }
   Print(texto);
}

//Desenha os textos no Chart para informacao
void display(){  
    if( !IsTesting() || DebugMode){
      string display = "  Sauna Pips v1.03\n";
      display = StringConcatenate( display, " ----------------------------------------\n" );
      display = StringConcatenate( display, "  Leverage: ", DoubleToStr( AccountLeverage(), 0 ), " Lots: ", DoubleToStr( lotSize, 2 ), ", \n" ); 
      display = StringConcatenate( display, "  Avg. Spread: ", DoubleToStr( avgSpread / Point, 0 ), " of ", MaxSpread, ", \n" ); 
      display = StringConcatenate( display, "  Commission: ", DoubleToStr( commissionPoints / Point, 0 ), " \n" ); 
      display = StringConcatenate( display, "  Broker StopLevel: ", IntegerToString(stoplevel), " \n" ); 
      
      display = StringConcatenate( display, "  GMT Now: ", niceHour( gmt ), " \n" ); 
      display = StringConcatenate( display, " ----------------------------------------\n" );
      display = StringConcatenate( display, "  Set: ", TradeComment, " \n" ); 
      display = StringConcatenate( display, " ----------------------------------------\n" );         
      display = StringConcatenate( display, "  Velocity: ", DoubleToStr( rateChange / Point, 0 ), " \n" ); 
      
      
      if(DebugMode){
         display = StringConcatenate( display, " ----------------------------------------\n" );
         display = StringConcatenate( display, " Total Buy Trades: " , IntegerToString(totalBuy), " \n" );
         display = StringConcatenate( display, " Total Sell Trades: " , IntegerToString(totalSell), " \n" );
         display = StringConcatenate( display, " TotalPending Trades: " , IntegerToString(totalBuyStop+totalSellStop), " \n" );
         display = StringConcatenate( display, " RateChange: ", DoubleToStr( rateChange, Digits() ), " \n" );
         display = StringConcatenate( display, " TimeCurrent: " , IntegerToString((int) TimeCurrent()), " \n" );
      
         string string_arraySpread;
         for( int i = 0; i < ArraySize(spreadSize); i++ ){
            string_arraySpread = StringConcatenate(string_arraySpread, DoubleToStr( spreadSize[i] / Point, 0 ), ",");
         }
         display = StringConcatenate( display, "Array Spread: ", string_arraySpread, " \n" );
         display = StringConcatenate( display, "Use Dynamic StopLoss: " , IntegerToString(useDynamicStopLoss), " \n" );
         if(useDynamicStopLoss){
            for( int i = 0; i < ArraySize(dynamicSLTicket); i++ ) {
               display = StringConcatenate( display, "[",i,"]: ", IntegerToString(dynamicSLTicket[i])," -> ",DoubleToStr(dynamicSL[i], Digits())," \n" );
            }
         }
      }
      
      Comment( display ); 
  }
} 