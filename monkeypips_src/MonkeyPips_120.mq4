#property copyright "Copyright trevone"
#property link "https://www.mql5.com/en/users/trevone" 
#property strict
#property version "1.20"

enum manager {
   P=0, // Primary
   B=1, // Secondary
};

extern string LicenseKey = "9676DA2D3E8A737E7501A2FD1026E0E6D507509165ECD5A5";
extern manager TradeManager = 0;
extern string TradeComment = "MonkeyPips v1.20";
extern string BreakoutComment = "MonkeyPips Breakout v1.20"; 
extern int MagicNumber = 33431; 
extern int BreakoutMagic = 33432;
extern int Slippage = 3;
extern int MaxSpread = 9;
extern double FixedLot = 0;
extern int RiskPercent = 10;
extern int GMTOffset = 0; 
extern int StartHour = 23; 
extern int EndHour = 20; 
extern int TradeDeviation = 5;  
extern int TradeDelta = 9;   
extern int OrderDelta = 60; 
extern int Trailing = 4; 
extern int TrailTime = 5; 
extern int VelocityTrigger = 50;
extern int ReverseTrigger = 30;
extern int VelocityTime = 7; 
extern int ReverseTime = 4; 
extern int OrderTime = 29;

int MagicAlive = 621475380; 
string AliveComment = "Just here to keep session alive";

int lastBuyVelocityTime, lastSellVelocityTime;

int OrderExpiry = 15; 
int TickSample = 100;  
int r, gmt, brokerOffset, size, digits, stoplevel, lastAlive;

double marginRequirement, maxLot, minLot, lotSize, points, currentSpread, avgSpread, maxSpread, initialBalance, rateChange, rateTrigger;

double spreadSize[]; 
double tick[];
int tickTime[];   

string testerStartDate, testerEndDate;

bool timeToTrade;
bool licenseValid = false;  
bool hasKey = false; 
string licensetext;

int timeBuy, timeSell;

int init() {  
   hasKey = false;
   authorise();
   marginRequirement = MarketInfo( Symbol(), MODE_MARGINREQUIRED ) * 0.01;
   maxLot = ( double ) MarketInfo( Symbol(), MODE_MAXLOT );  
   minLot = ( double ) MarketInfo( Symbol(), MODE_MINLOT );  
   currentSpread = NormalizeDouble( Ask - Bid, Digits ); 
   stoplevel = ( int ) MathMax( MarketInfo( Symbol(), MODE_FREEZELEVEL ), MarketInfo( Symbol(), MODE_STOPLEVEL ) );
   avgSpread = currentSpread; 
   size = TickSample;
   ArrayResize( spreadSize, size ); 
   ArrayFill( spreadSize, 0, size, avgSpread );
   maxSpread = NormalizeDouble( MaxSpread * Point, Digits );
   rateTrigger = NormalizeDouble( ( double ) VelocityTrigger * Point, Digits );
   testerStartDate = StringConcatenate( Year(), "-", Month(), "-", Day() );
   initialBalance = AccountBalance(); 
   lastAlive = ( int ) TimeCurrent();
   display();
   return ( 0 );
}

void OnDeinit( const int reason ){ 
   for( int pos = 0; pos < OrdersTotal(); pos++ ) {
      r = OrderSelect( pos, SELECT_BY_POS, MODE_TRADES );
      if( OrderSymbol() != Symbol() ) continue;   
      if( OrderMagicNumber() == MagicAlive ){
         r = OrderDelete( OrderTicket() );
      }
   }
} 
 
int start() {   
   int totalBuyStop = 0;
   int totalSellStop = 0;  
   
   int totalBuyStop2 = 0;
   int totalSellStop2 = 0;
   
   int totalBuy = 0;
   int totalSell = 0; 
   
   int totalTrades = 0;
   int ticket; 
   
   int buyTicket = 0;
   int sellTicket = 0; 
   
   prepareTimeToTrade();
   prepareSpread();
   manageTicks();  
   int orderExpiry = ( int ) TimeCurrent() + ( 60 * OrderExpiry ); 
   for( int pos = 0; pos < OrdersTotal(); pos++ ) {
      r = OrderSelect( pos, SELECT_BY_POS, MODE_TRADES );
      if( OrderSymbol() != Symbol() ) continue;   
      if( OrderMagicNumber() == MagicNumber ){ 
         switch ( OrderType() ) {
            case OP_BUYSTOP:
               if( avgSpread > maxSpread ||  MathAbs( rateChange ) < VelocityTrigger * Point  ) 
                  r = OrderDelete( OrderTicket() );
               totalBuyStop++;
            break;
            case OP_SELLSTOP:
               if( avgSpread > maxSpread ||  MathAbs( rateChange ) < VelocityTrigger * Point   ) 
                  r = OrderDelete( OrderTicket() );
               totalSellStop++;
            break;
            case OP_BUY:  
              // if( ( int ) TimeCurrent() - ( int ) OrderOpenTime() < TrailTime ){ 
                  if( OrderStopLoss() == 0.0 || OrderStopLoss() <= OrderOpenPrice() ){
                     if( Bid - OrderOpenPrice() > Trailing * Point ){
                        if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                           if( NormalizeDouble( Bid - ( Trailing * Point ), Digits ) != OrderStopLoss() )
                              r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
                     } else {
                        if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                           if( NormalizeDouble( Bid - ( Trailing * Point ), Digits ) != OrderStopLoss() )
                           r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
                     }
                  } else {
                     if( ( int ) TimeCurrent() - lastBuyVelocityTime > ReverseTime )
                        if( Bid - OrderOpenPrice() > Trailing * Point ){
                           if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                              if( NormalizeDouble( Bid - ( Trailing * Point ), Digits ) != OrderStopLoss() )
                                 r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
                        } else {
                           if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                              if( NormalizeDouble( Bid - ( Trailing * Point ), Digits ) != OrderStopLoss() )
                              r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );
                        }          
                  } 
              // } else { 
                  
              // }
               totalTrades++;
            break;
            case OP_SELL:  
               //if( ( int ) TimeCurrent() - ( int ) OrderOpenTime() < TrailTime ){
                  if( OrderStopLoss() == 0.0 || OrderStopLoss() >= OrderOpenPrice() ){
                     if( OrderOpenPrice() - Ask > Trailing * Point ){
                        if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                           if( NormalizeDouble( Ask + ( Trailing * Point ), Digits ) != OrderStopLoss() )
                              r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );
                     } else {
                        if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                           if( NormalizeDouble( Ask + ( Trailing * Point ), Digits ) != OrderStopLoss() )
                              r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );
                     }
                  } else {
                     if ( ( int ) TimeCurrent() - lastSellVelocityTime > ReverseTime )
                        if( OrderOpenPrice() - Ask > Trailing * Point ){
                           if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                              if( NormalizeDouble( Ask + ( Trailing * Point ), Digits ) != OrderStopLoss() )
                                 r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );
                        } else {
                           if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                              if( NormalizeDouble( Ask + ( Trailing * Point ), Digits ) != OrderStopLoss() )
                                 r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );
                        }
                  }
               //} else { 
                   
               //}  
               totalTrades++;
            break;
         } 
      } else if( OrderMagicNumber() == BreakoutMagic ){ 
         switch ( OrderType() ) {
            case OP_BUYSTOP:
               if( TradeManager == 0 ) {
                  if( ( int ) TimeCurrent() - timeBuy > OrderTime ){
                     if( NormalizeDouble( Ask + ( Point * OrderDelta ), Digits ) != OrderOpenPrice() )
                        r = OrderModify(  OrderTicket(), NormalizeDouble( Ask + ( Point * OrderDelta ), Digits ), 0, 0, 0 ); 
                        if( r ){
                           timeBuy = ( int ) TimeCurrent();
                        }
                  }
               }
               buyTicket = OrderTicket();
               totalBuyStop2++;
            break;
            case OP_SELLSTOP:
               if( TradeManager == 0 ) {
                  if( ( int ) TimeCurrent() - timeSell > OrderTime ){
                     if( NormalizeDouble( Bid - ( Point * OrderDelta ), Digits ) != OrderOpenPrice() )
                        r = OrderModify( OrderTicket(), NormalizeDouble( Bid - ( Point * OrderDelta ), Digits ), 0, 0, 0 ); 
                        if( r ){
                           timeSell = ( int ) TimeCurrent();
                        }
                  }
               }
               sellTicket = OrderTicket();
               totalSellStop2++;
            break;
            case OP_BUY:  
               //if( ( int ) TimeCurrent() - ( int ) OrderOpenTime() < TrailTime ){ 
                  if( OrderStopLoss() == 0.0 || OrderStopLoss() <= OrderOpenPrice() ){
                     if( Bid - OrderOpenPrice() > Trailing * Point ){
                        if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                           if( NormalizeDouble( Bid - ( Trailing * Point ), Digits ) != OrderStopLoss() )
                              r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
                     } else {
                        if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                           if( NormalizeDouble( Bid - ( Trailing * Point ), Digits ) != OrderStopLoss() )
                              r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
                     }
                  } else {
                     if( ( int ) TimeCurrent() - lastBuyVelocityTime > ReverseTime )
                        if( Bid - OrderOpenPrice() > Trailing * Point ){
                           if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                              if( NormalizeDouble( Bid - ( Trailing * Point ), Digits ) != OrderStopLoss() )
                                 r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
                        } else {
                           if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                              if( NormalizeDouble( Bid - ( Trailing * Point ), Digits ) != OrderStopLoss() )
                                 r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
                        }         
                  } 
               //} else { 
                  
               //} 
               totalBuy++;
            break;
            case OP_SELL:  
               //if( ( int ) TimeCurrent() - ( int ) OrderOpenTime() < TrailTime ){
                  if( OrderStopLoss() == 0.0 || OrderStopLoss() >= OrderOpenPrice() ){
                     if( OrderOpenPrice() - Ask > Trailing * Point ){
                        if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                           if( NormalizeDouble( Ask + ( Trailing * Point ), Digits ) != OrderStopLoss() )
                              r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );
                     } else {
                        if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                     if( NormalizeDouble( Ask + ( Trailing * Point ), Digits ) != OrderStopLoss() )
                        r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
                     }
                  } else {
                     if ( ( int ) TimeCurrent() - lastSellVelocityTime > ReverseTime)
                        if( OrderOpenPrice() - Ask > Trailing * Point ){
                           if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                              if( NormalizeDouble( Ask + ( Trailing * Point ), Digits ) != OrderStopLoss() )
                                 r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );
                        } else {
                           if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                              if( NormalizeDouble( Ask + ( Trailing * Point ), Digits ) != OrderStopLoss() )
                                 r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );
                        }
                  }
               //} else { 
                   
               //}
               totalSell++;
            break;
         } 
      }
   }  
    
   if( TradeManager == 0 ) {
      if( timeToTrade ){ 
         if( rateChange > VelocityTrigger * Point && avgSpread <= maxSpread && totalBuyStop < TradeDeviation ) {
            ticket = OrderSend(Symbol(), OP_BUYSTOP, lotSize(), Ask + ( totalBuyStop + 1.0 ) * ( Point * TradeDelta ), Slippage, 0, 0, TradeComment, MagicNumber, orderExpiry );
         } 
         if( rateChange < -VelocityTrigger * Point && avgSpread <= maxSpread && totalSellStop < TradeDeviation ) {
            ticket = OrderSend(Symbol(), OP_SELLSTOP, lotSize(), Bid - ( totalSellStop + 1.0 ) * ( Point * TradeDelta ), Slippage, 0, 0, TradeComment, MagicNumber, orderExpiry );
         }   
       
         if( ( int ) TimeCurrent() - lastSellVelocityTime < ReverseTime && rateChange > VelocityTrigger * Point && avgSpread <= maxSpread && totalBuyStop < TradeDeviation ) {
            ticket = OrderSend(Symbol(), OP_BUYSTOP, lotSize(), Ask + ( totalBuyStop + 1.0 ) * ( Point * TradeDelta ), Slippage, 0, 0, TradeComment, MagicNumber, orderExpiry );
         } 
         if( ( int ) TimeCurrent() - lastBuyVelocityTime < ReverseTime && rateChange < -VelocityTrigger * Point && avgSpread <= maxSpread && totalSellStop < TradeDeviation ) {
            ticket = OrderSend(Symbol(), OP_SELLSTOP, lotSize(), Bid - ( totalSellStop + 1.0 ) * ( Point * TradeDelta ), Slippage, 0, 0, TradeComment, MagicNumber, orderExpiry );
         } 
         
         if( timeToTrade ){
            if( avgSpread <= maxSpread ){
               if( totalBuyStop2 == 0 && totalBuy == 0 && totalSell == 0 ){
                  ticket = OrderSend(Symbol(), OP_BUYSTOP, lotSize(), Ask + ( Point * OrderDelta ), 3, 0, 0, BreakoutComment, BreakoutMagic );
                  if( ticket ){
                     timeBuy = ( int ) TimeCurrent();
                  }
               }
               if( totalSellStop2 == 0 && totalSell == 0 && totalBuy == 0 ){
                  ticket = OrderSend(Symbol(), OP_SELLSTOP, lotSize(), Bid - ( Point * OrderDelta ), 3, 0, 0, BreakoutComment, BreakoutMagic );
                  if( ticket ){
                     timeSell = ( int ) TimeCurrent();
                  }
               }   
            }  
         } 
      }   
   } 
    
   display(); 
   return ( 0 );
} 

double lotSize(){ 
 
      if( FixedLot > 0 ){
         lotSize = NormalizeDouble( FixedLot, 2 );
      } else {
         if( marginRequirement > 0 ) 
            lotSize = MathMax( MathMin( NormalizeDouble( ( AccountBalance() * ( ( double ) RiskPercent / 1000 ) * 0.01 / marginRequirement ), 2 ), maxLot ), minLot );    
      } 
 
   return ( lotSize ); 
}  

void prepareSpread(){
   if( !IsTesting() ){  
      double spreadSize_temp[];
      ArrayResize( spreadSize_temp, size - 1 );
      ArrayCopy( spreadSize_temp, spreadSize, 0, 1, size - 1 );
      ArrayResize( spreadSize_temp, size ); 
      spreadSize_temp[size-1] = NormalizeDouble( Ask - Bid, Digits ); 
      ArrayCopy( spreadSize, spreadSize_temp, 0, 0 ); 
      avgSpread = iMAOnArray( spreadSize, size, size, 0, MODE_LWMA, 0 );  
   }
}   

bool prepareTimeToTrade(){  
   gmt = offlineGMT();
   if( ( ( StartHour < EndHour && gmt >= StartHour && gmt <= EndHour ) 
      || ( StartHour > EndHour && ( ( gmt <= EndHour && gmt >= 0 ) 
      || ( gmt <= 23 && gmt >= StartHour ) ) ) ) 
      ){ 
      timeToTrade = true;
      return ( true );
   } else {
      timeToTrade = false; 
      return ( false );
   }
}

string niceHour( int kgmt ){ 
   string m;
   if( kgmt > 12 ) {
      kgmt = kgmt - 12;
      m = "PM"; 
   } else {
      m = "AM";
      if( kgmt == 12 )
         m = "PM";
   }
   return ( StringConcatenate( kgmt, m ) );
}

void prepareGMT(){
   int gmtH = gmt;
   int bkrH = Hour();
   brokerOffset =  bkrH - gmtH; 
   if( brokerOffset < 0 ) brokerOffset += 24;
   else if( brokerOffset > 23 ) brokerOffset -= 24;
}

int offlineGMT(){
   int bkrH = Hour();
   int gOffset =  bkrH - GMTOffset; 
   if( gOffset < 0 ) gOffset += 24;
   else if( gOffset > 23 ) gOffset -= 24;
   return ( gOffset );
}

string ArrayToHex(uchar &arre[],int count=-1){
   string res="";
   if(count<0 || count>ArraySize(arre))
      count=ArraySize(arre);
   for(int i=0; i<count; i++)
      res+=StringFormat( "%.2X",arre[i] );
   return(res);
}

void authorise(){
   int valid = 0;  
   licenseValid = decodeKey();
   licensetext = "";
   if( licenseValid == 1 ){   
      hasKey = true;
      return;
   } else licensetext = "Notice: Invalid Key\n  Limited lot size of 0.01";    
}
 
int decodeKey(){
   int valid = 0;
   int acc = AccountNumber(); 
   string text = StringConcatenate( acc, "monkeypips.v116" ); 
   string keystr = "6r$gDj)0sK";
   uchar src[], dst[],key[]; 
   StringToCharArray( keystr, key ); 
   StringToCharArray( text, src );  
   int res = CryptEncode( CRYPT_DES, src, key, dst );  
   if( res > 0 ){     
      if( ArrayToHex( dst ) == LicenseKey ){ 
         valid = 1;
      }
   } else Print( "Error in CryptEncode. Error code=", GetLastError() );
   return ( valid );
} 

int gmtHour( int inH ){
   int outH;
   if( IsTesting() ) return ( inH );
   outH = inH - brokerOffset;
   if( outH < 0 ) outH += 24;
   else if( outH > 23 ) outH -= 24;
   return ( outH );
}

void manageTicks(){    
   double tick_temp[], tickTime_temp[];
   ArrayResize( tick_temp, size - 1 );
   ArrayResize( tickTime_temp, size - 1 );
   ArrayCopy( tick_temp, tick, 0, 1, size - 1 ); 
   ArrayCopy( tickTime_temp, tickTime, 0, 1, size - 1 ); 
   ArrayResize( tick_temp, size ); 
   ArrayResize( tickTime_temp, size );
   tick_temp[size-1] = Bid;
   tickTime_temp[size-1] = ( int ) TimeCurrent();
   ArrayCopy( tick, tick_temp, 0, 0 );    
   ArrayCopy( tickTime, tickTime_temp, 0, 0 ); 
   int timeNow = tickTime[size-1];
   double priceNow = tick[size-1];
   double priceThen = 0;   
   for( int i = size - 1; i >= 0; i-- ){ 
      if( timeNow - tickTime[i] > VelocityTime ){
         priceThen = tick[i]; 
         break;
      }  
   }     
   rateChange = ( priceNow - priceThen );
   if( rateChange / Point > 5000 ) rateChange = 0;  
   if( rateChange > ReverseTrigger * Point ) lastBuyVelocityTime = ( int ) TimeCurrent();
   if( rateChange < -ReverseTrigger * Point ) lastSellVelocityTime = ( int ) TimeCurrent();
} 

void display(){  
   if( !IsTesting() ){ 
      string display = "  Monkey Pips v1.20\n";
      display = StringConcatenate( display, " ----------------------------------------\n" );
      if( TradeManager == 0 )
         display = StringConcatenate( display, "  TradeManager: Primary\n" );
      else 
         display = StringConcatenate( display, "  TradeManager: Secondary\n" );
      display = StringConcatenate( display, " ----------------------------------------\n" );
      display = StringConcatenate( display, "  GMT: ", niceHour( gmt ), "\n" );
      display = StringConcatenate( display, " ----------------------------------------\n" );
      display = StringConcatenate( display, "  Leverage: ", DoubleToStr( AccountLeverage(), 0 ), " Lots: ", DoubleToStr( lotSize, 2 ), ", \n" ); 
      display = StringConcatenate( display, "  Avg. Spread: ", DoubleToStr( avgSpread / Point, 0 ), " of ", MaxSpread, ", \n" ); 
      display = StringConcatenate( display, " ----------------------------------------\n" );
      display = StringConcatenate( display, "  Set: ", TradeComment, " \n" ); 
      display = StringConcatenate( display, " ----------------------------------------\n" ); 
      if( !hasKey ){
         display = StringConcatenate( display, "  ", licensetext, "\n" );
         display = StringConcatenate( display, "  Mail: trevorschuil@yahoo.com\n" );
      }  
      if( stoplevel > 0 ){
         display = StringConcatenate( display, "  Error: Broker not supported\n" );
      }
      Comment( display ); 
   }
} 