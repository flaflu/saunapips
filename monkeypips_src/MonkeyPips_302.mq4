#property copyright "Copyright trevone"
#property link "https://www.mql5.com/en/users/trevone" 
#property strict
#property version "2.03"

enum manager {
   P=0, // Primary
   B=1, // Secondary
}; 

extern manager TradeManager = 0;
extern string TradeComment = "MonkeyPips v3.01"; 
extern int MagicNumber = 33431; 
extern int Slippage = 3;
extern int MaxSpread = 18;
extern double FixedLot = 0;
extern int RiskPercent = 30; 
extern int MaxDrawdown = 90;
extern int TradeDeviation = 3;  
extern int TradeDelta = 12;
extern int Trailing = 3;  
extern int TrailingLoss = 7;  
extern int VelocityTrigger = 70;
extern int VelocityStop = 40; 
extern int VelocityTime = 7;  
extern int DeleteRatio = 30;  

int OrderExpiry = 15; 
int TickSample = 100;  
int r, gmt, brokerOffset, size, digits, stoplevel;

double marginRequirement, maxLot, minLot, lotSize, points, currentSpread, avgSpread, maxSpread, initialBalance, rateChange, rateTrigger, deleteRatio, commissionPoints;

double spreadSize[]; 
double tick[];
double avgtick[];
int tickTime[];   

string testerStartDate, testerEndDate; 

int lastBuyOrder, lastSellOrder;

bool calculateCommission = true;

double max = 0;

int init() {   
   marginRequirement = MarketInfo( Symbol(), MODE_MARGINREQUIRED ) * 0.01;
   maxLot = ( double ) MarketInfo( Symbol(), MODE_MAXLOT );  
   minLot = ( double ) MarketInfo( Symbol(), MODE_MINLOT );  
   currentSpread = NormalizeDouble( Ask - Bid, Digits ); 
   stoplevel = ( int ) MathMax( MarketInfo( Symbol(), MODE_FREEZELEVEL ), MarketInfo( Symbol(), MODE_STOPLEVEL ) );
   if( stoplevel > TradeDelta ) TradeDelta = stoplevel;
   if( stoplevel > Trailing ) Trailing = stoplevel;
   avgSpread = currentSpread; 
   size = TickSample;
   ArrayResize( spreadSize, size ); 
   ArrayFill( spreadSize, 0, size, avgSpread );
   maxSpread = NormalizeDouble( MaxSpread * Point, Digits );
   deleteRatio = NormalizeDouble( ( double ) DeleteRatio / 100, 2 );
   rateTrigger = NormalizeDouble( ( double ) VelocityTrigger * Point, Digits );
   testerStartDate = StringConcatenate( Year(), "-", Month(), "-", Day() );
   initialBalance = AccountBalance();  
   display();
   return ( 0 );
}

void OnDeinit( const int reason ){ 
 
} 

void commission(){ 
   if( !IsTesting() ){ 
      double rate = 0;
      for( int pos = OrdersHistoryTotal() - 1; pos >= 0; pos-- ) {
         if( OrderSelect( pos, SELECT_BY_POS, MODE_HISTORY ) ) {
            if( OrderProfit() != 0.0 ) {
               if( OrderClosePrice() != OrderOpenPrice() ) {
                  if( OrderSymbol() == Symbol() ) {
                     calculateCommission = false;
                     rate = MathAbs( OrderProfit() / MathAbs( OrderClosePrice() - OrderOpenPrice() ) );
                     commissionPoints = ( -OrderCommission() ) / rate;
                     break;
                  }
               }
            }
         }
      } 
   }
}

int start() {   
   int totalBuyStop = 0;
   int totalSellStop = 0;  
   int ticket;  
   if( calculateCommission ) commission();
   prepareSpread();
   manageTicks(); 
   int totalTrades = 0;
   for( int pos = 0; pos < OrdersTotal(); pos++ ) {
      r = OrderSelect( pos, SELECT_BY_POS, MODE_TRADES );
      if( OrderSymbol() != Symbol() ) continue;   
      if( OrderMagicNumber() == MagicNumber ){ 
         totalTrades++;
         switch ( OrderType() ) {
            case OP_BUYSTOP:
               if( (int) TimeCurrent() - lastBuyOrder > VelocityTime && rateChange < VelocityTrigger * Point * deleteRatio )
                  r = OrderDelete( OrderTicket() );
               totalBuyStop++;
            break;
            case OP_SELLSTOP:
               if( (int) TimeCurrent() - lastSellOrder > VelocityTime && rateChange > -VelocityTrigger * Point * deleteRatio )
                  r = OrderDelete( OrderTicket() );
               totalSellStop++;
            break;
            case OP_BUY:    
               if( Bid - OrderOpenPrice() + commissionPoints > Trailing * Point ){  
                  if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                     if( NormalizeDouble( Bid - ( Trailing * Point ), Digits ) != OrderStopLoss() )
                        r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );                  
               } else {
                  if( AccountEquity() > max || AccountEquity() / AccountBalance() < ( double ) MaxDrawdown / 100 ){
                     if( rateChange < -VelocityStop * Point && Bid < OrderOpenPrice() - ( VelocityTrigger * Point ) )
                        if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > ( Trailing * Point * TrailingLoss ) )
                           if( NormalizeDouble( Bid - ( Trailing * Point * TrailingLoss ), Digits ) != OrderStopLoss() )
                              r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point * TrailingLoss ), Digits ), OrderTakeProfit(), 0 ); 
                  }
               } 
            break;
            case OP_SELL:    
               if( OrderOpenPrice() - commissionPoints - Ask > Trailing * Point ){  
                  if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                     if( NormalizeDouble( Ask + ( Trailing * Point ), Digits ) != OrderStopLoss() )
                        r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );                    
               } else {
                  if( AccountEquity() > max || AccountEquity() / AccountBalance() < ( double ) MaxDrawdown / 100 ){
                     if( rateChange > VelocityStop * Point  && Ask > OrderOpenPrice() + ( VelocityTrigger * Point ) )
                        if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > ( Trailing * Point * TrailingLoss ) ) 
                           if( NormalizeDouble( Ask + ( Trailing * Point * TrailingLoss ), Digits ) != OrderStopLoss() )
                              r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point * TrailingLoss ), Digits ), OrderTakeProfit(), 0 );
                  }
               } 
            break;
         }  
      }
   }  
   
   if( totalTrades == 0 ){
      if( AccountBalance() > max ) max = AccountBalance();
   }
    
   if( TradeManager == 0 && totalTrades < TradeDeviation ) { 
      if( rateChange > VelocityTrigger * Point && avgSpread <= maxSpread && totalBuyStop < TradeDeviation ) {
         if( AccountFreeMarginCheck( Symbol(), OP_BUY, lotSize() ) <= 0 || GetLastError() == 134 ) {
            Print( "Notice: Not enough free margin to open trade. Expert is removed." );
            ExpertRemove();
            return ( 0 ); 
         }
         ticket = OrderSend( Symbol(), OP_BUYSTOP, lotSize(), Ask + ( totalBuyStop + 1.0 ) * ( Point * TradeDelta ), Slippage, 0, 0, TradeComment, MagicNumber, 0 );
         lastBuyOrder = ( int ) TimeCurrent();
      } 
      if( rateChange < -VelocityTrigger * Point && avgSpread <= maxSpread && totalSellStop < TradeDeviation ) {
         if( AccountFreeMarginCheck( Symbol(), OP_SELL, lotSize() ) <= 0 || GetLastError() == 134 ) {
            Print( "Notice: Not enough free margin to open trade. Expert is removed." );
            ExpertRemove();
            return ( 0 );
         }
         ticket = OrderSend(Symbol(), OP_SELLSTOP, lotSize(), Bid - ( totalSellStop + 1.0 ) * ( Point * TradeDelta ), Slippage, 0, 0, TradeComment, MagicNumber, 0 );
         lastSellOrder = ( int ) TimeCurrent();
      }    
   }  
    
   display(); 
   return ( 0 );
} 

double lotSize(){  
   if( FixedLot > 0 ){
      lotSize = NormalizeDouble( FixedLot, 2 );
   } else {
      if( marginRequirement > 0 ) 
         lotSize = MathMax( MathMin( NormalizeDouble( ( AccountBalance() * ( ( double ) RiskPercent / 1000 ) * 0.01 / marginRequirement ), 2 ), maxLot ), minLot );    
   }  
   return ( NormalizeLots( lotSize ) ); 
}  

double NormalizeLots( double p ){
    double ls = MarketInfo( Symbol(), MODE_LOTSTEP );
    return( MathRound( p / ls ) * ls );
}

void prepareSpread(){
   if( !IsTesting() ){  
      double spreadSize_temp[];
      ArrayResize( spreadSize_temp, size - 1 );
      ArrayCopy( spreadSize_temp, spreadSize, 0, 1, size - 1 );
      ArrayResize( spreadSize_temp, size ); 
      spreadSize_temp[size-1] = NormalizeDouble( Ask - Bid, Digits ); 
      ArrayCopy( spreadSize, spreadSize_temp, 0, 0 ); 
      avgSpread = iMAOnArray( spreadSize, size, size, 0, MODE_LWMA, 0 );  
   }
}       

void manageTicks(){    
   double tick_temp[], tickTime_temp[], avgtick_temp[];
   ArrayResize( tick_temp, size - 1 );
   ArrayResize( tickTime_temp, size - 1 );
   ArrayCopy( tick_temp, tick, 0, 1, size - 1 ); 
   ArrayCopy( tickTime_temp, tickTime, 0, 1, size - 1 ); 
   ArrayResize( tick_temp, size ); 
   ArrayResize( tickTime_temp, size );
   tick_temp[size-1] = Bid;
   tickTime_temp[size-1] = ( int ) TimeCurrent();
   ArrayCopy( tick, tick_temp, 0, 0 );    
   ArrayCopy( tickTime, tickTime_temp, 0, 0 ); 
   int timeNow = tickTime[size-1];
   double priceNow = tick[size-1];
   double priceThen = 0;   
   int period = 0;
   for( int i = size - 1; i >= 0; i-- ){ 
      period++;
      if( timeNow - tickTime[i] > VelocityTime ){
         priceThen = tick[i]; 
         break;
      }  
   }     
    
   rateChange = ( priceNow - priceThen );
   if( rateChange / Point > 5000 ) rateChange = 0;     
} 

void display(){  
    if( !IsTesting() ){ 
      string display = "  Monkey Pips v3.01\n";
      display = StringConcatenate( display, " ----------------------------------------\n" );
      if( TradeManager == 0 )
         display = StringConcatenate( display, "  TradeManager: Primary\n" );
      else 
         display = StringConcatenate( display, "  TradeManager: Secondary\n" ); 
      display = StringConcatenate( display, " ----------------------------------------\n" );
      display = StringConcatenate( display, "  Leverage: ", DoubleToStr( AccountLeverage(), 0 ), " Lots: ", DoubleToStr( lotSize, 2 ), ", \n" ); 
      display = StringConcatenate( display, "  Avg. Spread: ", DoubleToStr( avgSpread / Point, 0 ), " of ", MaxSpread, ", \n" ); 
      display = StringConcatenate( display, "  Commission: ", DoubleToStr( commissionPoints / Point, 0 ), " \n" ); 
      display = StringConcatenate( display, " ----------------------------------------\n" );
      display = StringConcatenate( display, "  Set: ", TradeComment, " \n" ); 
      display = StringConcatenate( display, " ----------------------------------------\n" );  
      if( AccountFreeMarginCheck( Symbol(), OP_BUY, lotSize() ) <= 0 || GetLastError() == 134 ) {
         display = StringConcatenate( display, "  Notice: Not enough free margin to open trade.\n  Expert is removed. \n" );  
      }
      Comment( display ); 
  }
}

double OnTester(){ 
   if( ( double ) TesterStatistics( STAT_EXPECTED_PAYOFF ) > 0 && ( double) TesterStatistics( STAT_PROFIT ) > 0 ){  
      testerEndDate = StringConcatenate( Year(), "-", Month(), "-", Day() );
      char post[],char_result[];
      string cookie = NULL, headers; 
      int res;     
      
      string params = StringConcatenate(   
         "TradeComment=", TradeComment, ";",  
         "MaxSpread=", DoubleToStr( MaxSpread, 0 ), ";",
         "RiskPercent=", DoubleToStr( RiskPercent, 0 ), ";",  
         "TradeDeviation=", DoubleToStr( TradeDeviation, 0 ), ";",
         "TradeDelta=", DoubleToStr( TradeDelta, 0 ), ";",  
         "Trailing=", DoubleToStr( Trailing, 0 ), ";", 
         "TrailingLoss=", DoubleToStr( TrailingLoss, 0 ), ";",
         "VelocityTrigger=", DoubleToStr( VelocityTrigger, 0 ), ";",
         "VelocityStop=", DoubleToStr( VelocityStop, 0 ), ";",
         "VelocityTime=", DoubleToStr( VelocityTime, 0 ), ";"
      );              
      
      double profitTrades = TesterStatistics( STAT_PROFIT_TRADES );
      double losingTrades = TesterStatistics( STAT_LOSS_TRADES );
      double winRatio = NormalizeDouble( profitTrades / ( profitTrades + losingTrades ) * 100, 2 );
      
      string url = StringConcatenate( "http://my.domain.com/mt4/onTester.php?symbol=", 
         Symbol(), 
         "&balance=", initialBalance, 
         "&spread=", DoubleToStr( ( Ask - Bid ) / Point, 0 ), 
         "&start=", testerStartDate, 
         "&end=", testerEndDate, 
         "&leverage=", AccountLeverage(), 
         "&profit=", TesterStatistics( STAT_PROFIT ), 
         "&totalTrades=", TesterStatistics( STAT_TRADES ), 
         "&profitFactor=", TesterStatistics( STAT_PROFIT_FACTOR ), 
         "&expectedPayoff=", TesterStatistics( STAT_EXPECTED_PAYOFF ), 
         "&drawdownDollar=", TesterStatistics( STAT_EQUITY_DD_RELATIVE ), 
         "&drawdownPercent=", TesterStatistics( STAT_EQUITY_DDREL_PERCENT ), 
         "&winRatio=", winRatio,
         "&settings=", params ); 

      ResetLastError(); 
      int timeout = 5000; 
      res = WebRequest( "GET", url, cookie, NULL, timeout, post, 0, char_result, headers );
      return( ( double ) CharArrayToString( char_result, 0, WHOLE_ARRAY,CP_ACP ) ); 
   } else return ( 0 );
} 