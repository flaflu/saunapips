#property copyright "Copyright trevone"
#property link "https://www.mql5.com/en/users/trevone" 
#property strict
#property version "1.17"

enum manager {
   P=0, // Primary
   B=1, // Secondary
};  

extern string LicenseKey = "9676DA2D3E8A737E7501A2FD1026E0E6D507509165ECD5A5";
extern manager TradeManager = 0;
string VelocityComment = "MonkeyPips Velocity v1.17";
string BreakoutComment = "MonkeyPips Breakout v1.17";
string ReversalComment = "MonkeyPips Breakout v1.17";
int VelocityMagic = 33431;
int BreakoutMagic = 33432; 
int ReversalMagic = 6778; 
extern bool VelocityStrategy = true; // Velocity Strategy
extern bool BreakoutStrategy = true; // Breakout Strategy
extern bool ReversalStrategy = true; // Reversal Strategy
extern int MaxSpread = 9;
extern double FixedLot = 0;
extern int RiskPercent = 10;
extern int GMTOffset = 0; 
extern int StartHour = 23; 
extern int EndHour = 20;  
extern int Trailing = 6; 
extern int TrailTime = 120;   
extern int StopLoss = 25; 
extern int TradeDeviation = 4;  
extern int TradeDelta = 7;
extern int VelocityTrigger = 40; 
extern int OrderDelta = 60;
extern int OrderTime = 30; 
extern int CandleFilter = 90;
extern int OrderLimit = 6; 

int VelocityTime = 5; 
int OrderExpiry = 15; 
int TickSample = 100;  
int r, gmt, brokerOffset, size, digits;

double marginRequirement, maxLot, minLot, lotSize, points, currentSpread, avgSpread, maxSpread, initialBalance, rateChange, rateTrigger;

double spreadSize[]; 
double tick[];
int tickTime[];   

string testerStartDate, testerEndDate;

bool timeToTrade;
int timeBuy, timeSell;
bool allowTrade = false;
bool licenseValid = false;
int yearExpiry, monthExpiry;

bool hasKey = false;

double limitedLots;
string licensetext; 

int MAPeriod = 3;
int MAMethod = 3; 
  
int precision ;
double maxSpreadPlusCommission, negStop, lotstep, fixed ;

bool calculateCommission = true; 

int Gi_180 = 0; 
int G_digits_192 = 0;
double G_point_196 = 0.0;
int Gi_204;
double Gd_208;
double Gd_216;
double Gd_224; 
double Gd_240;
double Gd_248;
double Gd_256;
double Gd_257;
int G_slippage_264 = 3;  

int init() {  
   hasKey = false;
   authorise();
   marginRequirement = MarketInfo( Symbol(), MODE_MARGINREQUIRED ) * 0.01;
   maxLot = ( double ) MarketInfo( Symbol(), MODE_MAXLOT );  
   minLot = ( double ) MarketInfo( Symbol(), MODE_MINLOT );  
   currentSpread = NormalizeDouble( Ask - Bid, Digits ); 
   avgSpread = currentSpread; 
   size = TickSample;
   ArrayResize( spreadSize, size ); 
   ArrayFill( spreadSize, 0, size, avgSpread );
   maxSpread = NormalizeDouble( MaxSpread * Point, Digits );
   rateTrigger = NormalizeDouble( ( double ) VelocityTrigger * Point, Digits );
   testerStartDate = StringConcatenate( Year(), "-", Month(), "-", Day() );
   initialBalance = AccountBalance(); 
   
   
   HideTestIndicators( true );   
   size = 100;
   lotstep = MarketInfo( Symbol(), MODE_LOTSTEP );
   precision = ( int ) ( MathLog( lotstep ) / MathLog( 0.1 ) );
   marginRequirement = MarketInfo( Symbol(), MODE_MARGINREQUIRED ) * lotstep; 
   maxLot = NormalizeDouble( ( double ) MarketInfo( Symbol(), MODE_MAXLOT ), 2 );  
   minLot = MathMax( NormalizeDouble( ( double ) MarketInfo( Symbol(), MODE_MINLOT ), 2 ), lotstep ); 
   fixed = MathMax( MathMin( NormalizeDouble( ( double ) FixedLot / 100, precision ), maxLot ), minLot ); 
   currentSpread = NormalizeDouble( Ask - Bid, Digits ); 
   avgSpread = currentSpread; 
   ArrayResize( spreadSize, size ); 
   ArrayFill( spreadSize, 0, size, avgSpread );
   G_digits_192 = Digits;
   G_point_196 = Point;
   double lotstep_0 = MarketInfo( Symbol(), MODE_LOTSTEP ); 
   Gd_240 = NormalizeDouble( OrderLimit * G_point_196, G_digits_192 );
   Gd_248 = NormalizeDouble( Trailing * G_point_196, G_digits_192 );
   Gd_256 = NormalizeDouble( CandleFilter * G_point_196, G_digits_192 );  
   Gd_257 = NormalizeDouble( StopLoss * G_point_196, G_digits_192 ); 
   testerStartDate = StringConcatenate( Year(), "-", Month(), "-", Day() );
   initialBalance = AccountBalance(); 
   
   return ( 0 );
} 


string ArrayToHex(uchar &arre[],int count=-1){
   string res="";
   if(count<0 || count>ArraySize(arre))
      count=ArraySize(arre);
   for(int i=0; i<count; i++)
      res+=StringFormat( "%.2X",arre[i] );
   return(res);
}

void authorise(){
   int valid = 0;  
   licenseValid = decodeKey();
   licensetext = "";
   if( licenseValid == 1 ){   
      hasKey = true;
      return;
   } else licensetext = "Error: Key is not valid.";    
}
 
int decodeKey(){
   int valid = 0;
   int acc = AccountNumber(); 
   string text = StringConcatenate( acc, "monkeypips.v116" ); 
   string keystr = "6r$gDj)0sK";
   uchar src[], dst[],key[]; 
   StringToCharArray( keystr, key ); 
   StringToCharArray( text, src );  
   int res = CryptEncode( CRYPT_DES, src, key, dst );  
   if( res > 0 ){     
      if( ArrayToHex( dst ) == LicenseKey ){ 
         valid = 1;
      }
   } else Print( "Error in CryptEncode. Error code=", GetLastError() );
   return ( valid );
} 
 
int start() {   
   int totalBuyStop = 0;
   int totalSellStop = 0;
   int totalBuyStop2 = 0;
   int totalSellStop2 = 0;
   int totalBuy = 0;
   int totalSell = 0;  
   int buyTicket = 0;
   int sellTicket = 0;  
   int ticket; 
   prepareTimeToTrade();
   prepareSpread();
   manageTicks();  
   int orderExpiry = ( int ) TimeCurrent() + ( 60 * OrderExpiry ); 
    
   int ticket_20;
   double price_24;
   bool bool_32;
   double Ld_36;
   double Ld_44;
   double price_60; 
   int Li_180;
   int cmd_188;  
   
   double ihigh_68 = iHigh( NULL, 0, 0 );
   double ilow_76 = iLow( NULL, 0, 0 );
   double ima_84 = iMA( NULL, 0, MAPeriod, Gi_180, MAMethod, PRICE_LOW, 0 );
   double ima_92 = iMA( NULL, 0, MAPeriod, Gi_180, MAMethod, PRICE_HIGH, 0 );
   double Ld_100 = ima_84 - ima_92;  
   
   double Ld_172 = ihigh_68 - ilow_76;
   Li_180 = 0;
   if ( Ld_172 > Gd_256 ) {
      if( Bid < ima_84 ) Li_180 = -1;
      else if( Bid > ima_92 ) Li_180 = 1;
   }
   int count_184 = 0; 
   
   for( int pos = 0; pos < OrdersTotal(); pos++ ) {
      r = OrderSelect( pos, SELECT_BY_POS, MODE_TRADES );
      if( OrderSymbol() != Symbol() ) continue;   
      if( OrderMagicNumber() == VelocityMagic ){ 
         switch ( OrderType() ) {
            case OP_BUYSTOP:
               if( avgSpread > maxSpread || MathAbs( rateChange ) < VelocityTrigger * Point ) r = OrderDelete( OrderTicket() );
               totalBuyStop++;
            break;
            case OP_SELLSTOP:
               if( avgSpread > maxSpread || MathAbs( rateChange ) < VelocityTrigger * Point ) r = OrderDelete( OrderTicket() );
               totalSellStop++;
            break;
            case OP_BUY:  
               if( ( int ) TimeCurrent() - ( int ) OrderOpenTime() < TrailTime ){
                  if( Bid - OrderOpenPrice() > Trailing * Point )
                     if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                        r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
               } else {
                  if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                     r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
               }
            break;
            case OP_SELL:  
               if( ( int ) TimeCurrent() - ( int ) OrderOpenTime() < TrailTime ){
                  if( OrderOpenPrice() - Ask > Trailing * Point )
                     if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                        r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );
               } else {
                  if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                     r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
               }  
            break;
         } 
      } else if( OrderMagicNumber() == BreakoutMagic ){ 
         switch ( OrderType() ) {
            case OP_BUYSTOP:
               if( ( int ) TimeCurrent() - timeBuy > OrderTime ){
                  r = OrderModify( OrderTicket(), Ask + ( Point * OrderDelta ), 0, 0, 0 ); 
                  if( r ){
                     timeBuy = ( int ) TimeCurrent();
                  }
               }
               buyTicket = OrderTicket();
               totalBuyStop2++;
            break;
            case OP_SELLSTOP:
               if( ( int ) TimeCurrent() - timeSell > OrderTime ){
                  r = OrderModify( OrderTicket(), Bid - ( Point * OrderDelta ), 0, 0, 0 ); 
                  if( r ){
                     timeSell = ( int ) TimeCurrent();
                  }
               }
               sellTicket = OrderTicket();
               totalSellStop2++;
            break;
            case OP_BUY:  
               if( ( int ) TimeCurrent() - ( int ) OrderOpenTime() < TrailTime ){
                  if( Bid - OrderOpenPrice() > Trailing * Point )
                     if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                        r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
               } else {
                  if( OrderStopLoss() == 0.0 || Bid - OrderStopLoss() > Trailing * Point )
                     r = OrderModify( OrderTicket(), OrderOpenPrice(), NormalizeDouble( Bid - ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
               } 
               totalBuy++;
            break;
            case OP_SELL:  
               if( ( int ) TimeCurrent() - ( int ) OrderOpenTime() < TrailTime ){
                  if( OrderOpenPrice() - Ask > Trailing * Point )
                     if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                        r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 );
               } else {
                  if( OrderStopLoss() == 0.0 || OrderStopLoss() - Ask > Trailing * Point ) 
                     r = OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble( Ask + ( Trailing * Point ), Digits ), OrderTakeProfit(), 0 ); 
               }  
               totalSell++;
            break;
         } 
      } else if( OrderMagicNumber() == ReversalMagic ){ 
         cmd_188 = OrderType(); 
         count_184++;
         switch( cmd_188 ) {
         case OP_BUY:
            if( Trailing < 0 ) break;
            Ld_44 = NormalizeDouble( OrderStopLoss(), G_digits_192 );
            if( Bid - Gd_248 > OrderOpenPrice() ){
               price_60 = NormalizeDouble( Bid - Gd_248, G_digits_192 );
            } else {
               price_60 = NormalizeDouble( Bid - Gd_257, G_digits_192 );
            }
            if( !( ( Ld_44 == 0.0 || price_60 > Ld_44 ) ) ) break;
            bool_32 = OrderModify( OrderTicket(), OrderOpenPrice(), price_60, OrderTakeProfit(), 0 );
            if ( !( !bool_32 ) ) break; 
            break;
         case OP_SELL:
            if( Trailing < 0 ) break;
            Ld_44 = NormalizeDouble( OrderStopLoss(), G_digits_192 );
            if( Ask + Gd_248 < OrderOpenPrice()  ){
               price_60 = NormalizeDouble( Ask + Gd_248, G_digits_192 );
            } else {
               price_60 = NormalizeDouble( Ask + Gd_257, G_digits_192 );
            }
            if( !( ( Ld_44 == 0.0 || price_60 < Ld_44 ) ) ) break;
            bool_32 = OrderModify( OrderTicket(), OrderOpenPrice(), price_60, OrderTakeProfit(), 0 );
            if( !( !bool_32 ) ) break; 
            break;
         case OP_BUYSTOP:
            Ld_36 = NormalizeDouble( OrderOpenPrice(), G_digits_192 );
            price_24 = NormalizeDouble( Ask + Gd_240, G_digits_192 );
            if( !( ( price_24 < Ld_36 ) ) ) break;
            price_60 = NormalizeDouble( price_24 - StopLoss * Point, G_digits_192 );
            bool_32 = OrderModify( OrderTicket(), price_24, price_60, OrderTakeProfit(), 0 );
            if( !( !bool_32 ) ) break; 
            break;
         case OP_SELLSTOP:
            Ld_36 = NormalizeDouble( OrderOpenPrice(), G_digits_192 );
            price_24 = NormalizeDouble( Bid - Gd_240, G_digits_192 );
            if( !( ( price_24 > Ld_36 ) ) ) break;
            price_60 = NormalizeDouble( price_24 + StopLoss * Point, G_digits_192 );
            bool_32 = OrderModify( OrderTicket(), price_24, price_60, OrderTakeProfit(), 0 );
            if( !( !bool_32 ) ) break; 
         } 
      }
   }   
 
  // if( hasKey || IsTesting() ){
      if( TradeManager == 0 ) {
         if( timeToTrade ){ 
            if( VelocityStrategy ){
               if( rateChange > VelocityTrigger * Point && avgSpread <= maxSpread && totalBuyStop < TradeDeviation ) {
                  ticket = OrderSend(Symbol(), OP_BUYSTOP, lotSize(), Ask + ( totalBuyStop + 1.0 ) * ( Point * TradeDelta ), 3, 0, 0, VelocityComment, VelocityMagic, orderExpiry );
               } 
               if( rateChange < -VelocityTrigger * Point && avgSpread <= maxSpread && totalSellStop < TradeDeviation ) {
                  ticket = OrderSend(Symbol(), OP_SELLSTOP, lotSize(), Bid - ( totalSellStop + 1.0 ) * ( Point * TradeDelta ), 3, 0, 0, VelocityComment, VelocityMagic, orderExpiry );
               }    
            }
            if( BreakoutStrategy ){
               if( avgSpread <= maxSpread ){
                  if( totalBuyStop2 == 0 && totalBuy == 0 && totalSell == 0 ){
                     ticket = OrderSend(Symbol(), OP_BUYSTOP, lotSize(), Ask + ( Point * OrderDelta ), 3, 0, 0, BreakoutComment, BreakoutMagic );
                     if( ticket ){
                        timeBuy = ( int ) TimeCurrent();
                     }
                  }
                  if( totalSellStop2 == 0 && totalSell == 0 && totalBuy == 0 ){
                     ticket = OrderSend(Symbol(), OP_SELLSTOP, lotSize(), Bid - ( Point * OrderDelta ), 3, 0, 0, BreakoutComment, BreakoutMagic );
                     if( ticket ){
                        timeSell = ( int ) TimeCurrent();
                     }
                  }   
               }  
            }
            if( ReversalStrategy ){
               if( count_184 == 0 && Li_180 != 0 && avgSpread <= maxSpread ) { 
                  if( Li_180 < 0 ) {
                     price_24 = NormalizeDouble( Ask + Gd_240, G_digits_192 );
                     price_60 = NormalizeDouble( price_24 - StopLoss * Point, G_digits_192 );
                     ticket_20 = OrderSend( Symbol(), OP_BUYSTOP, lotSize(), price_24, G_slippage_264, price_60, 0, ReversalComment, ReversalMagic, 0 ); 
                  } else {
                     price_24 = NormalizeDouble( Bid - Gd_240, G_digits_192 );
                     price_60 = NormalizeDouble( price_24 + StopLoss * Point, G_digits_192 );
                     ticket_20 = OrderSend( Symbol(), OP_SELLSTOP, lotSize(), price_24, G_slippage_264, price_60, 0, ReversalComment, ReversalMagic, 0 ); 
                  }
               } 
            }
         }    
      }
   //}  
   display();
   ObjectsDeleteAll();
   return (0);
} 

double lotSize(){ 
   if( FixedLot > 0 ){
      lotSize = NormalizeDouble( FixedLot, 2 );
   } else {
      if( marginRequirement > 0 ) 
         lotSize = MathMax( MathMin( NormalizeDouble( ( AccountBalance() * ( ( double ) RiskPercent / 1000 ) * 0.01 / marginRequirement ), 2 ), maxLot ), minLot );    
   } 
   return ( lotSize ); 
}  

void prepareSpread(){
   if( !IsTesting() ){  
      double spreadSize_temp[];
      ArrayResize( spreadSize_temp, size - 1 );
      ArrayCopy( spreadSize_temp, spreadSize, 0, 1, size - 1 );
      ArrayResize( spreadSize_temp, size ); 
      spreadSize_temp[size-1] = NormalizeDouble( Ask - Bid, Digits ); 
      ArrayCopy( spreadSize, spreadSize_temp, 0, 0 ); 
      avgSpread = iMAOnArray( spreadSize, size, size, 0, MODE_LWMA, 0 );  
   }
}   

bool prepareTimeToTrade(){  
   gmt = offlineGMT();
   if( ( ( StartHour < EndHour && gmt >= StartHour && gmt <= EndHour ) 
      || ( StartHour > EndHour && ( ( gmt <= EndHour && gmt >= 0 ) 
      || ( gmt <= 23 && gmt >= StartHour ) ) ) ) 
      ){ 
      timeToTrade = true;
      return ( true );
   } else {
      timeToTrade = false; 
      return ( false );
   }
}

string niceHour( int kgmt ){ 
   string m;
   if( kgmt > 12 ) {
      kgmt = kgmt - 12;
      m = "PM"; 
   } else {
      m = "AM";
      if( kgmt == 12 )
         m = "PM";
   }
   return ( StringConcatenate( kgmt, m ) );
}

void prepareGMT(){
   int gmtH = gmt;
   int bkrH = Hour();
   brokerOffset =  bkrH - gmtH; 
   if( brokerOffset < 0 ) brokerOffset += 24;
   else if( brokerOffset > 23 ) brokerOffset -= 24;
}

int offlineGMT(){
   int bkrH = Hour();
   int gOffset =  bkrH - GMTOffset; 
   if( gOffset < 0 ) gOffset += 24;
   else if( gOffset > 23 ) gOffset -= 24;
   return ( gOffset );
}

int gmtHour( int inH ){
   int outH;
   if( IsTesting() ) return ( inH );
   outH = inH - brokerOffset;
   if( outH < 0 ) outH += 24;
   else if( outH > 23 ) outH -= 24;
   return ( outH );
}

void manageTicks(){    
   double tick_temp[], tickTime_temp[];
   ArrayResize( tick_temp, size - 1 );
   ArrayResize( tickTime_temp, size - 1 );
   ArrayCopy( tick_temp, tick, 0, 1, size - 1 ); 
   ArrayCopy( tickTime_temp, tickTime, 0, 1, size - 1 ); 
   ArrayResize( tick_temp, size ); 
   ArrayResize( tickTime_temp, size );
   tick_temp[size-1] = Bid;
   tickTime_temp[size-1] = ( int ) TimeCurrent();
   ArrayCopy( tick, tick_temp, 0, 0 );    
   ArrayCopy( tickTime, tickTime_temp, 0, 0 ); 
   int timeNow = tickTime[size-1];
   double priceNow = tick[size-1];
   double priceThen = 0;   
   for( int i = size - 1; i >= 0; i-- ){ 
      if( timeNow - tickTime[i] > VelocityTime ){
         priceThen = tick[i]; 
         break;
      }  
   }     
   rateChange = ( priceNow - priceThen );
   if( rateChange / Point > 5000 ) rateChange = 0;  
} 

void display(){  
   if( !IsTesting() ){ 
      string display = "  Monkey Pips v1.17\n";
      display = StringConcatenate( display, " ----------------------------------------\n" );
      if( TradeManager == 0 )
         display = StringConcatenate( display, "  TradeManager: Primary\n" );
      else 
         display = StringConcatenate( display, "  TradeManager: Secondary\n" );
      display = StringConcatenate( display, " ----------------------------------------\n" );
      display = StringConcatenate( display, "  GMT: ", niceHour( gmt ), "\n" );
      display = StringConcatenate( display, " ----------------------------------------\n" );
      display = StringConcatenate( display, "  Leverage: ", DoubleToStr( AccountLeverage(), 0 ), " Lots: ", DoubleToStr( lotSize, 2 ), ", \n" ); 
      display = StringConcatenate( display, "  Avg. Spread: ", DoubleToStr( avgSpread / Point, 0 ), " of ", MaxSpread, ", \n" ); 
      display = StringConcatenate( display, " ----------------------------------------\n" );
      display = StringConcatenate( display, "  INSTRUCTIONS\n" );
      display = StringConcatenate( display, "    Must be logged into the same account with 2 different MT4 terminals.\n" );
      display = StringConcatenate( display, "    Terminal 1: Add to the charts you want to trade and set TradeManager to Primary.\n" ); 
      display = StringConcatenate( display, "    Terminal 2: Add to the same charts as Terminal 1 and set TradeManager to Secondary.\n" );
      display = StringConcatenate( display, " ----------------------------------------\n" );
      //if( !hasKey ){
         //display = StringConcatenate( display, "  ", licensetext, "\n" );
         //display = StringConcatenate( display, "  Mail trevorschuil@yahoo.com\n" );
      //}
      Comment( display ); 
   }
} 